import { RootState } from 'app'

export const getSystemSelector = (state: RootState) => state.systems.getSystem

export const createSystemSelector = (state: RootState) =>
  state.systems.createSystem

export const editSystemSelector = (state: RootState) => state.systems.editSystem
