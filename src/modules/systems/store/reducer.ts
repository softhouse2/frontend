import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { System } from 'shared/types'
import {
  createSystem,
  editSystem,
  getSystem,
  resetCreateSystem,
  resetEditSystem,
} from './actions'

export interface State {
  getSystem: StateObject<System>
  createSystem: StateObject<System>
  editSystem: StateObject<System>
}

const initialState: State = {
  getSystem: stateObject.getInitial(),
  createSystem: stateObject.getInitial(),
  editSystem: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getSystem.pending, state => {
      stateObject.setPending(state.getSystem)
    })
    .addCase(getSystem.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getSystem, action.payload)
    })
    .addCase(getSystem.rejected, (state, action) => {
      stateObject.setFailed(state.getSystem, action.error.message)
    })
    .addCase(createSystem.pending, state => {
      stateObject.setPending(state.createSystem)
    })
    .addCase(createSystem.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createSystem, action.payload)
    })
    .addCase(createSystem.rejected, (state, action) => {
      stateObject.setFailed(state.createSystem, action.error.message)
    })
    .addCase(resetCreateSystem, state => {
      stateObject.reset(state.createSystem, initialState.createSystem.data)
    })
    .addCase(editSystem.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editSystem, action.payload)
    })
    .addCase(editSystem.rejected, (state, action) => {
      stateObject.setFailed(state.editSystem, action.error.message)
    })
    .addCase(resetEditSystem, state => {
      stateObject.reset(state.editSystem, initialState.editSystem.data)
    })
)
