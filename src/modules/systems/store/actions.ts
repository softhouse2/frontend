import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import Systems, { CreateSystemParams } from 'shared/services/systems'
import { System } from 'shared/types'

const systems = new Systems()

export const getSystem = createAsyncThunk('getSystem', (systemId: string) =>
  systems.getSystem(systemId)
)

export const createSystem = createAsyncThunk(
  'createSystem',
  (payload: CreateSystemParams) => systems.createSystem(payload)
)

export const editSystem = createAsyncThunk('editSystem', (payload: System) =>
  systems.editSystem(payload)
)

export const resetCreateSystem = createAction('resetCreateSystem')
export const resetEditSystem = createAction('resetEditSystem')
