import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.createSystem,
    element: Loadable({
      component: () => import('./pages/CreateSystem'),
    }),
  },
  {
    path: paths.systems,
    element: Loadable({
      component: () => import('./pages/Systems'),
    }),
  },
  {
    path: paths.editSystem,
    element: Loadable({
      component: () => import('./pages/EditSystem'),
    }),
  },
]

export default routes
