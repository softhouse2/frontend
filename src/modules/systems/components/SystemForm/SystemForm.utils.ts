import paths from 'config/paths'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { System, LoadingStatus } from 'shared/types'
import { object, SchemaOf, string } from 'yup'
import { actions, selectors } from '../../store'

export enum SystemFormFields {
  Name = 'name',
}

export type SystemFormValues = Omit<System, 'id'>

export const systemFormDefaultValues: SystemFormValues = {
  [SystemFormFields.Name]: '',
}

export const systemFormSchema = (): SchemaOf<SystemFormValues> =>
  object()
    .shape({
      [SystemFormFields.Name]: string()
        .min(3, 'Name must have at least 3 characers')
        .max(40, 'Name cannot be longer than 40 characters')
        .required(),
    })
    .required()

export const useCreateSystem = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const createSystem = (system: SystemFormValues) => {
    dispatch(actions.createSystem(system))
  }

  const { loading } = useSelector(selectors.createSystemSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.systems, { replace: true })
    return () => {
      dispatch(actions.resetCreateSystem())
    }
  }, [dispatch, loading, navigate])

  return {
    createSystem,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditSystem = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { systemId } = useParams()
  const editSystem = (system: SystemFormValues) => {
    if (!systemId) return
    const parsedId = parseInt(systemId)
    dispatch(actions.editSystem({ id: parsedId, ...system }))
  }

  const { loading } = useSelector(selectors.editSystemSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.systems, { replace: true })
    return () => {
      dispatch(actions.resetEditSystem())
    }
  }, [dispatch, loading, navigate])

  return {
    editSystem,
    isEditing: loading === LoadingStatus.Pending,
  }
}
