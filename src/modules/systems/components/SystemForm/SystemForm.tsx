import { Formik } from 'formik'
import { Card } from 'react-bootstrap'
import { LoadingButton, StyledForm, TextField } from 'shared/components'
import { System } from 'shared/types'
import {
  systemFormDefaultValues,
  SystemFormFields,
  systemFormSchema,
  SystemFormValues,
} from './SystemForm.utils'

interface SystemFormProps {
  onSubmit: (system: SystemFormValues) => void
  loading: boolean
  system?: System
  title: string
}

const SystemForm = ({ onSubmit, loading, system, title }: SystemFormProps) => {
  return (
    <Formik
      initialValues={system ? system : systemFormDefaultValues}
      validationSchema={systemFormSchema}
      onSubmit={onSubmit}
    >
      <StyledForm>
        <Card className="p3">
          <Card.Body>
            <Card.Title>{title}</Card.Title>
            <TextField
              name={SystemFormFields.Name}
              label="System name"
            ></TextField>
            <LoadingButton
              variant="secondary"
              size="lg"
              type="submit"
              loading={loading}
            >
              Save
            </LoadingButton>
          </Card.Body>
        </Card>
      </StyledForm>
    </Formik>
  )
}

export default SystemForm
