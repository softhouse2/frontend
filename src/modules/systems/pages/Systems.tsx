import paths from 'config/paths'
import { useEffect } from 'react'
import { Button } from 'react-bootstrap'
import { PersonPlus } from 'react-bootstrap-icons'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate } from 'react-router-dom'
import { PageTitle } from 'shared/components'
import { DataTable, Column } from 'shared/components/DataTable'
import { actions, selectors } from 'shared/store'
import styled from 'styled-components'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 30px;
`
const Systems = () => {
  const columns: Column[] = [{ field: 'name', label: 'System name' }]

  const { data } = useSelector(selectors.systems.getSystemsSelector)

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.systems.getSystems({}))
  }, []) //eslint-disable-line

  const onRowClick = (systemId: string) => {
    navigate(generatePath(paths.editSystem, { systemId }))
  }

  return (
    <>
      <Header>
        <PageTitle title="Systems" />
        <Button
          type="button"
          onClick={() => navigate(paths.createSystem)}
          variant="outline-primary"
        >
          <PersonPlus size="30px" className="me-3" />
          Create System
        </Button>
      </Header>
      <DataTable columns={columns} data={data} onRowClick={onRowClick} />
    </>
  )
}

export default Systems
