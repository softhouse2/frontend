import { FormPageContainer } from 'shared/components'
import { SystemForm, useCreateSystem } from '../components'

const CreateSystem = () => {
  const { createSystem, isCreating } = useCreateSystem()
  return (
    <FormPageContainer>
      <SystemForm
        onSubmit={createSystem}
        loading={isCreating}
        title={'Create System'}
      />
    </FormPageContainer>
  )
}

export default CreateSystem
