import { actions, selectors } from 'modules/systems/store'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { FormPageContainer } from 'shared/components'
import { LoadingStatus } from 'shared/types'
import { SystemForm, useEditSystem } from '../components'

const EditSystem = () => {
  const { editSystem, isEditing } = useEditSystem()
  const dispatch = useDispatch()
  const { systemId } = useParams()
  const { data: system, loading } = useSelector(selectors.getSystemSelector)

  useEffect(() => {
    if (!systemId) return
    dispatch(actions.getSystem(systemId))
  }, []) //eslint-disable-line

  return loading !== LoadingStatus.Succeeded ? null : (
    <FormPageContainer>
      <SystemForm
        onSubmit={editSystem}
        loading={isEditing}
        system={system}
        title="Edit System"
      />
    </FormPageContainer>
  )
}

export default EditSystem
