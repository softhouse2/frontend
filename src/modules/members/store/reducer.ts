import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Member } from 'shared/types'
import {
  createMember,
  editMember,
  getMember,
  resetCreateMember,
  resetEditMember,
} from './actions'

export interface State {
  getMember: StateObject<Member>
  createMember: StateObject<Member>
  editMember: StateObject<Member>
}

const initialState: State = {
  getMember: stateObject.getInitial(),
  createMember: stateObject.getInitial(),
  editMember: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getMember.pending, state => {
      stateObject.setPending(state.getMember)
    })
    .addCase(getMember.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getMember, action.payload)
    })
    .addCase(getMember.rejected, (state, action) => {
      stateObject.setFailed(state.getMember, action.error.message)
    })
    .addCase(createMember.pending, state => {
      stateObject.setPending(state.createMember)
    })
    .addCase(createMember.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createMember, action.payload)
    })
    .addCase(createMember.rejected, (state, action) => {
      stateObject.setFailed(state.createMember, action.error.message)
    })
    .addCase(resetCreateMember, state => {
      stateObject.reset(state.createMember, initialState.createMember.data)
    })
    .addCase(editMember.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editMember, action.payload)
    })
    .addCase(editMember.rejected, (state, action) => {
      stateObject.setFailed(state.editMember, action.error.message)
    })
    .addCase(resetEditMember, state => {
      stateObject.reset(state.editMember, initialState.editMember.data)
    })
)
