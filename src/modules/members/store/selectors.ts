import { RootState } from 'app'

export const getMemberSelector = (state: RootState) => state.members.getMember

export const createMemberSelector = (state: RootState) =>
  state.members.createMember

export const editMemberSelector = (state: RootState) => state.members.editMember
