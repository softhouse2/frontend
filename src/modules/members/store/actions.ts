import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import Members, { CreateMemberParams } from 'shared/services/members'
import { Member } from 'shared/types'

const members = new Members()

export const getMember = createAsyncThunk('getMember', (memberId: string) =>
  members.getMember(memberId)
)

export const createMember = createAsyncThunk(
  'createMember',
  (payload: CreateMemberParams) => members.createMember(payload)
)

export const editMember = createAsyncThunk('editMember', (payload: Member) =>
  members.editMember(payload)
)

export const resetCreateMember = createAction('resetCreateMember')
export const resetEditMember = createAction('resetEditMember')
