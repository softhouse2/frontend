import paths from 'config/paths'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { LoadingStatus, Member, UserRole } from 'shared/types'
import { object, SchemaOf, string } from 'yup'
import { actions, selectors } from '../../store'

export enum MemberFormFields {
  Active = 'active',
  FirstName = 'firstName',
  LastName = 'lastName',
  Role = 'role',
  PhoneNumber = 'telNum',
  Email = 'email',
  Password = 'password',
}

export type MemberFormValues = Omit<Member, 'id'>

export const memberFormDefaultValues: MemberFormValues = {
  [MemberFormFields.Role]: UserRole.Admin,
  [MemberFormFields.Active]: true,
  [MemberFormFields.FirstName]: '',
  [MemberFormFields.LastName]: '',
  [MemberFormFields.PhoneNumber]: '',
  [MemberFormFields.Email]: '',
  [MemberFormFields.Password]: '',
}

export const memberFormSchema = (): SchemaOf<MemberFormValues> =>
  object()
    .shape({
      [MemberFormFields.FirstName]: string()
        .min(1, 'First name should have at least 1 character')
        .max(40, 'First name cannot be longer than 40 characters')
        .matches(/^[^0-9]+$/, 'First name cannot contain digits')
        .required('First name is required'),
      [MemberFormFields.LastName]: string()
        .min(1, 'Last name should have at least 1 character')
        .max(40, 'Last name cannot be longer than 40 characters')
        .matches(/^[^0-9]+$/, 'Last name cannot contain digits')
        .required('Last name is required'),
      [MemberFormFields.PhoneNumber]: string()
        .matches(/^[0-9]{9}$/, 'Phone number is not valid')
        .required('Phone number is required'),
      [MemberFormFields.Email]: string()
        .required('Email is required')
        .max(64, 'Email must be less than 64 characters')
        .email('Must be a valid email address'),
      [MemberFormFields.Password]: string()
        .min(4, 'Password should have at least 4 characters')
        .max(24, 'Password cannot be longer than 24 characters'),
    })
    .required()

export const useCreateMember = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const createMember = (member: MemberFormValues) => {
    dispatch(
      actions.createMember({
        ...member,
        active: member.active.toString() === 'true',
      })
    )
  }

  const { loading } = useSelector(selectors.createMemberSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.members, { replace: true })
    return () => {
      dispatch(actions.resetCreateMember())
    }
  }, [dispatch, loading, navigate])

  return {
    createMember,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditMember = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { memberId } = useParams()
  const editMember = (member: MemberFormValues) => {
    if (!memberId) return
    dispatch(
      actions.editMember({ id: memberId as unknown as number, ...member })
    )
  }

  const { loading } = useSelector(selectors.editMemberSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.members, { replace: true })
    return () => {
      dispatch(actions.resetEditMember())
    }
  }, [dispatch, loading, navigate])

  return {
    editMember,
    isEditing: loading === LoadingStatus.Pending,
  }
}
