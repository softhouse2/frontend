import { Card, Row, Col } from 'react-bootstrap'
import { Formik } from 'formik'
import { StyledForm } from 'shared/components'
import {
  MemberFormFields,
  MemberFormValues,
  memberFormDefaultValues,
  memberFormSchema,
} from './MemberForm.utils'
import { LoadingButton, TextField } from 'shared/components'
import Select from 'shared/components/Select'
import {
  Member,
  selectRoleOptions,
  selectActivityStatusOptions,
} from 'shared/types'

interface MemberFormProps {
  onSubmit: (member: MemberFormValues) => void
  loading: boolean
  member?: Member
  title: string
}

const MemberForm = ({ onSubmit, loading, member, title }: MemberFormProps) => {
  return (
    <Formik
      initialValues={member || memberFormDefaultValues}
      validationSchema={memberFormSchema}
      onSubmit={onSubmit}
    >
      {() => (
        <StyledForm>
          <Card className=" p-3">
            <Card.Body>
              <Card.Title>{title}</Card.Title>
              <Row className="mb-3">
                <Col>
                  <TextField
                    type="email"
                    placeholder="Enter email"
                    name={MemberFormFields.Email}
                    label="Email"
                  />
                </Col>
                <Col>
                  <Select
                    name={MemberFormFields.Role}
                    label="Role"
                    options={selectRoleOptions}
                    disabled={!!member}
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Col className="mb-3">
                  <TextField
                    type="text"
                    placeholder="Enter first name"
                    name={MemberFormFields.FirstName}
                    label="First name"
                  />
                  <TextField
                    type="text"
                    placeholder="Enter last name"
                    name={MemberFormFields.LastName}
                    label="Last name"
                  />
                </Col>
                <Col className="mb-3">
                  <TextField
                    type="text"
                    placeholder="Enter phone number"
                    name={MemberFormFields.PhoneNumber}
                    label="Phone number"
                  />
                  <Select
                    name={MemberFormFields.Active}
                    label="Status"
                    options={selectActivityStatusOptions}
                  />
                  <TextField
                    type="password"
                    placeholder="Enter password"
                    name={MemberFormFields.Password}
                    label="Password"
                  />
                </Col>
              </Row>
              <LoadingButton
                variant="secondary"
                size="lg"
                type="submit"
                loading={loading}
              >
                Save
              </LoadingButton>
            </Card.Body>
          </Card>
        </StyledForm>
      )}
    </Formik>
  )
}
export default MemberForm
