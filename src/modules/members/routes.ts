import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.createMember,
    element: Loadable({
      component: () => import('./pages/CreateMember'),
    }),
  },
  {
    path: paths.members,
    element: Loadable({
      component: () => import('./pages/Members'),
    }),
  },
  {
    path: paths.editMember,
    element: Loadable({
      component: () => import('./pages/EditMember'),
    }),
  },
]

export default routes
