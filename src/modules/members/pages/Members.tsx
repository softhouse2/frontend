import { Button } from 'react-bootstrap'
import { PageTitle, Column, DataTable } from 'shared/components'
import styled from 'styled-components'
import { PersonPlus } from 'react-bootstrap-icons'
import { generatePath, useNavigate } from 'react-router-dom'
import paths from 'config/paths'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { actions, selectors } from 'shared/store'
import { getRoleLabel } from 'shared/utils'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 30px;
`

const Members = () => {
  const columns: Column[] = [
    {
      field: 'firstName',
      label: 'First name',
    },
    {
      field: 'lastName',
      label: 'Last name',
    },
    {
      field: 'role',
      label: 'Role',
      renderValue: value => getRoleLabel(value),
    },
  ]

  const { data } = useSelector(selectors.members.getMembersSelector)

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.members.getMembers({}))
  }, []) //eslint-disable-line

  const onRowClick = (memberId: string) => {
    navigate(generatePath(paths.editMember, { memberId }))
  }

  return (
    <>
      <Header>
        <PageTitle title={'Members'} />
        <Button
          type="button"
          onClick={() => navigate(paths.createMember)}
          variant="outline-primary"
        >
          <PersonPlus size="30px" className="me-3" />
          Create Member
        </Button>
      </Header>
      <DataTable columns={columns} data={data} onRowClick={onRowClick} />
    </>
  )
}

export default Members
