import { FormPageContainer } from 'shared/components'
import { useCreateMember, MemberForm } from '../components'

const CreateMember = () => {
  const { createMember, isCreating } = useCreateMember()

  return (
    <FormPageContainer>
      <MemberForm
        onSubmit={createMember}
        loading={isCreating}
        title={'Create member'}
      />
    </FormPageContainer>
  )
}

export default CreateMember
