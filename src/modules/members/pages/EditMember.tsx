import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { FormPageContainer } from 'shared/components'
import { LoadingStatus } from 'shared/types'
import { useEditMember, MemberForm } from '../components'
import { actions, selectors } from '../store'

const EditMember = () => {
  const { editMember, isEditing } = useEditMember()
  const dispatch = useDispatch()
  const { memberId } = useParams()
  const { data: member, loading } = useSelector(selectors.getMemberSelector)

  useEffect(() => {
    if (!memberId) return
    dispatch(actions.getMember(memberId))
  }, []) //eslint-disable-line

  return loading !== LoadingStatus.Succeeded ? null : (
    <FormPageContainer>
      <MemberForm
        onSubmit={editMember}
        loading={isEditing}
        member={member}
        title={'Edit member'}
      />
    </FormPageContainer>
  )
}

export default EditMember
