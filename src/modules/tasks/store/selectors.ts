import { RootState } from 'app'

export const getTasksSelector = (state: RootState) => state.tasks.getTasks

export const getTaskSelector = (state: RootState) => state.tasks.getTask

export const createTaskSelector = (state: RootState) => state.tasks.createTask

export const editTaskSelector = (state: RootState) => state.tasks.editTask
