import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Task } from 'shared/types'
import {
  createTask,
  editTask,
  getTask,
  getTasks,
  resetEditTask,
  resetCreateTask,
} from './actions'

export interface State {
  getTasks: StateObject<Task[]>
  getTask: StateObject<Task>
  createTask: StateObject<Task>
  editTask: StateObject<Task>
}

const initialState: State = {
  getTasks: stateObject.getInitial([]),
  getTask: stateObject.getInitial(),
  createTask: stateObject.getInitial(),
  editTask: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getTasks.pending, state => {
      stateObject.setPending(state.getTasks)
    })
    .addCase(getTasks.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getTasks, action.payload)
    })
    .addCase(getTasks.rejected, (state, action) => {
      stateObject.setFailed(state.getTasks, action.error.message)
    })
    .addCase(getTask.pending, state => {
      stateObject.setPending(state.getTask)
    })
    .addCase(getTask.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getTask, action.payload)
    })
    .addCase(getTask.rejected, (state, action) => {
      stateObject.setFailed(state.getTask, action.error.message)
    })
    .addCase(createTask.pending, state => {
      stateObject.setPending(state.createTask)
    })
    .addCase(createTask.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createTask, action.payload)
    })
    .addCase(createTask.rejected, (state, action) => {
      stateObject.setFailed(state.createTask, action.error.message)
    })
    .addCase(resetCreateTask, state => {
      stateObject.reset(state.createTask, initialState.createTask.data)
    })
    .addCase(editTask.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editTask, action.payload)
    })
    .addCase(editTask.rejected, (state, action) => {
      stateObject.setFailed(state.editTask, action.error.message)
    })
    .addCase(resetEditTask, state => {
      stateObject.reset(state.editTask, initialState.editTask.data)
    })
)
