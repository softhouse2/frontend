import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import Tasks, {
  CreateTaskParams,
  EditTaskParams,
  GetTasksParams,
} from 'shared/services/tasks'

const tasks = new Tasks()

export const getTasks = createAsyncThunk(
  'getTasks',
  (payload: GetTasksParams) => tasks.getTasks(payload)
)

export const getTask = createAsyncThunk('getTask', (taskId: string) =>
  tasks.getTask(taskId)
)

export const createTask = createAsyncThunk(
  'createTask',
  (payload: CreateTaskParams) => tasks.createTask(payload)
)

export const editTask = createAsyncThunk(
  'editTask',
  (payload: EditTaskParams) => tasks.editTask(payload)
)

export const resetCreateTask = createAction('resetCreateTask')
export const resetEditTask = createAction('resetEditTask')
