import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate, useParams } from 'react-router-dom'
import { actions, selectors } from '../../store'
import { LoadingStatus, TaskType, WorkPriority, WorkStatus } from 'shared/types'
import { object, SchemaOf, string } from 'yup'
import { useEffect } from 'react'
import paths from 'config/paths'
import { formatDate } from 'shared/utils'
import { parseISO } from 'date-fns'

export enum TaskFormFields {
  TaskType = 'type',
  Priority = 'priority',
  Status = 'status',
  Result = 'result',
  Name = 'name',
  WorkerId = 'workerId',
  Deadline = 'deadline',
  DateOpened = 'dateOpened',
  DateInProgress = 'dateInProgress',
  DateClosed = 'dateClosed',
  IssueId = 'issueId',
}

export interface TaskFormValues {
  [TaskFormFields.TaskType]: TaskType
  [TaskFormFields.Priority]: WorkPriority
  [TaskFormFields.Status]: WorkStatus
  [TaskFormFields.Result]: string
  [TaskFormFields.Name]: string
  [TaskFormFields.WorkerId]: string
  [TaskFormFields.Deadline]: string
  [TaskFormFields.DateOpened]: string
  [TaskFormFields.DateInProgress]: string
  [TaskFormFields.DateClosed]: string
  [TaskFormFields.IssueId]: number
}

export const taskFormDefaultValues: TaskFormValues = {
  [TaskFormFields.TaskType]: TaskType.Programming,
  [TaskFormFields.Priority]: WorkPriority.NORMAL,
  [TaskFormFields.Status]: WorkStatus.Opened,
  [TaskFormFields.Result]: '',
  [TaskFormFields.Name]: '',
  [TaskFormFields.WorkerId]: '',
  [TaskFormFields.Deadline]: '',
  [TaskFormFields.DateOpened]: formatDate(new Date().toISOString()),
  [TaskFormFields.DateInProgress]: '',
  [TaskFormFields.DateClosed]: '',
  [TaskFormFields.IssueId]: 0,
}

export const taskFormSchema = (): SchemaOf<TaskFormValues> =>
  object()
    .shape({
      [TaskFormFields.Result]: string()
        .min(3, 'Result should have at least 3 characters')
        .max(512, 'Result should be no longer than 512 characters'),
      [TaskFormFields.Name]: string()
        .min(3, 'Name should have at least 3 characters')
        .max(128, 'Name should not be longer than 128 characters')
        .required('Name is required'),
      [TaskFormFields.WorkerId]: string().required('Required.'),
      [TaskFormFields.IssueId]: string().required('Required.'),
    })
    .required()

export const useCreateTask = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { requestId, issueId } = useParams()
  const createTask = (task: TaskFormValues) => {
    if (!issueId) return
    dispatch(
      actions.createTask({
        ...task,
        issueId: parseInt(issueId, 10),
        dateOpened: task.dateOpened
          ? parseISO(task.dateOpened).toISOString()
          : '',
        dateInProgress: task.dateInProgress
          ? parseISO(task.dateInProgress).toISOString()
          : '',
        dateClosed: task.dateClosed
          ? parseISO(task.dateClosed).toISOString()
          : '',
        deadline: task.deadline ? parseISO(task.deadline).toISOString() : '',
        workerId: Number(task.workerId),
      })
    )
  }

  const { loading } = useSelector(selectors.createTaskSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded) {
      navigate(generatePath(paths.editIssue, { issueId }), {
        replace: true,
      })
    }
  }, [dispatch, loading, navigate, issueId, requestId])

  useEffect(() => {
    return () => {
      dispatch(actions.resetCreateTask())
    }
  }, []) //eslint-disable-line

  return {
    createTask,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditTask = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { taskId } = useParams()
  const editTask = (task: TaskFormValues) => {
    if (!taskId) return
    dispatch(
      actions.editTask({
        id: taskId as unknown as number,
        ...task,
        dateOpened: task.dateOpened
          ? parseISO(task.dateOpened).toISOString()
          : '',
        dateInProgress: task.dateInProgress
          ? parseISO(task.dateInProgress).toISOString()
          : '',
        dateClosed: task.dateClosed
          ? parseISO(task.dateClosed).toISOString()
          : '',
        deadline: task.deadline ? parseISO(task.deadline).toISOString() : '',
        workerId: Number(task.workerId),
      })
    )
  }

  const { loading } = useSelector(selectors.editTaskSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded) navigate(-1)
  }, [dispatch, loading, navigate])

  useEffect(() => {
    return () => {
      dispatch(actions.resetEditTask())
    }
  }, []) //eslint-disable-line

  return {
    editTask,
    isEditing: loading === LoadingStatus.Pending,
  }
}
