import { addDays } from 'date-fns'
import { useFormikContext } from 'formik'
import { useEffect } from 'react'
import { Col, Row } from 'react-bootstrap'
import { TextField } from 'shared/components'
import { Task, WorkPriority, WorkStatus } from 'shared/types'
import { formatDate } from 'shared/utils'
import { TaskFormFields, TaskFormValues } from './TaskForm.utils'

interface DatesFieldsProps {
  task?: Task
}

const DatesFields = ({ task }: DatesFieldsProps) => {
  const { values, setFieldValue } = useFormikContext()

  const { priority, status } = values as TaskFormValues

  useEffect(() => {
    if (priority === task?.priority) {
      setFieldValue(TaskFormFields.Deadline, formatDate(task.deadline))
      return
    }
    switch (priority) {
      case WorkPriority.LOW:
        setFieldValue(
          TaskFormFields.Deadline,
          formatDate(addDays(new Date(), 14).toISOString())
        )
        break
      case WorkPriority.NORMAL:
        setFieldValue(
          TaskFormFields.Deadline,
          formatDate(addDays(new Date(), 7).toISOString())
        )
        break
      case WorkPriority.HIGH:
        setFieldValue(
          TaskFormFields.Deadline,
          formatDate(addDays(new Date(), 3).toISOString())
        )
        break
    }
  }, [priority]) //eslint-disable-line

  useEffect(() => {
    if (status === task?.status || status === WorkStatus.Opened) {
      setFieldValue(
        TaskFormFields.DateInProgress,
        task ? formatDate(task.dateInProgress) : ''
      )
      setFieldValue(
        TaskFormFields.DateClosed,
        task ? formatDate(task.dateClosed) : ''
      )
      return
    }
    switch (status) {
      case WorkStatus.InProgress:
        setFieldValue(
          TaskFormFields.DateInProgress,
          formatDate(new Date().toISOString())
        )
        setFieldValue(
          TaskFormFields.DateClosed,
          formatDate(task?.dateClosed) || ''
        )
        break
      case WorkStatus.Finished:
      case WorkStatus.Canceled:
        setFieldValue(
          TaskFormFields.DateClosed,
          formatDate(new Date().toISOString())
        )
        setFieldValue(
          TaskFormFields.DateInProgress,
          formatDate(task?.dateInProgress) || ''
        )
        break
    }
  }, [status]) //eslint-disable-line

  return (
    <>
      <Row className="mb-3">
        <Col>
          <TextField
            name={TaskFormFields.DateOpened}
            type="date"
            placeholder="Enter date opened"
            label="Open date"
            step="1"
            readOnly
          />
        </Col>
        <Col>
          <TextField
            name={TaskFormFields.Deadline}
            type="date"
            placeholder="Enter deadline"
            label="Deadline"
            step="1"
            readOnly
          />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <TextField
            name={TaskFormFields.DateInProgress}
            type="date"
            placeholder="Enter date in progress"
            label="Date in progress"
            step="1"
            readOnly
          />
        </Col>
        <Col>
          <TextField
            name={TaskFormFields.DateClosed}
            type="date"
            placeholder="Enter date finished"
            label="Date finished"
            step="1"
            readOnly
          />
        </Col>
      </Row>
    </>
  )
}

export default DatesFields
