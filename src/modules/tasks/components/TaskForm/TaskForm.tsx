import { Card, Row, Col } from 'react-bootstrap'
import { Formik } from 'formik'
import { StyledForm } from 'shared/components'
import { LoadingButton, TextField } from 'shared/components'
import Select from 'shared/components/Select'
import {
  selectStatusOptions,
  selectPriorityOptions,
  selectTaskTypeOptions,
  UserRole,
  Task,
} from 'shared/types'
import {
  TaskFormValues,
  TaskFormFields,
  taskFormDefaultValues,
  taskFormSchema,
} from './TaskForm.utils'
import { useDispatch, useSelector } from 'react-redux'
import { actions, selectors } from 'shared/store'
import { useEffect } from 'react'
import { formatDate, getSelectOptionsFromMembers } from 'shared/utils'
import DatesFields from './DatesFields'

interface TaskFormProps {
  onSubmit: (task: TaskFormValues) => void
  loading: boolean
  task?: Task
  title: string
  allowed?: boolean
}

const TaskForm = ({
  onSubmit,
  loading,
  task,
  title,
  allowed = true,
}: TaskFormProps) => {
  const dispatch = useDispatch()
  const { data: workers } = useSelector(selectors.members.getMembersSelector)
  useEffect(() => {
    dispatch(actions.members.getMembers({ role: UserRole.Worker }))
  }, []) //eslint-disable-line

  const selectWorkerOptions = getSelectOptionsFromMembers(workers)

  const initialValues = task
    ? {
        ...task,
        dateOpened: formatDate(task.dateOpened),
        deadline: formatDate(task.deadline),
        dateClosed: formatDate(task.dateClosed),
        dateInProgress: formatDate(task.dateInProgress),
        workerId: task.worker.id.toString(),
      }
    : taskFormDefaultValues

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={taskFormSchema}
      onSubmit={onSubmit}
    >
      {() => {
        return (
          <StyledForm>
            <Card className="p-3">
              <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Row className="mb-3">
                  <Col>
                    <Select
                      name={TaskFormFields.TaskType}
                      label="Type"
                      options={selectTaskTypeOptions}
                    />
                  </Col>
                  <Col>
                    <Select
                      name={TaskFormFields.Priority}
                      label="Priority"
                      options={selectPriorityOptions}
                    />
                  </Col>
                  <Col>
                    <Select
                      name={TaskFormFields.Status}
                      label="Status"
                      options={selectStatusOptions}
                    />
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <TextField
                      name={TaskFormFields.Name}
                      type="text"
                      placeholder="Enter task's name"
                      label="Task's name"
                    />
                  </Col>
                  <Col>
                    <Select
                      name={TaskFormFields.WorkerId}
                      label="Worker's name"
                      options={selectWorkerOptions}
                      pickFirstOption={!task}
                    />
                  </Col>
                </Row>
                <Card.Title>Task info</Card.Title>
                <Row className="mb-3">
                  <Col>
                    <TextField
                      name={TaskFormFields.Result}
                      label="Result"
                      as="textarea"
                    />
                  </Col>
                </Row>
                <DatesFields task={task} />
                <LoadingButton
                  variant="secondary"
                  size="lg"
                  type="submit"
                  loading={loading}
                  disabled={!allowed}
                >
                  Save
                </LoadingButton>
              </Card.Body>
            </Card>
          </StyledForm>
        )
      }}
    </Formik>
  )
}

export default TaskForm
