import { FormPageContainer } from 'shared/components'
import { TaskForm, useCreateTask } from '../components'

const CreateTask = () => {
  const { createTask, isCreating } = useCreateTask()

  return (
    <FormPageContainer>
      <TaskForm
        onSubmit={createTask}
        loading={isCreating}
        title={'Create task'}
      />
    </FormPageContainer>
  )
}

export default CreateTask
