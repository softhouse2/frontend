import {
  PageTitle,
  DataTable,
  Column,
  TableFilterValues,
  TableFilter,
} from 'shared/components'
import styled from 'styled-components'
import { format, parseISO } from 'date-fns'
import { generatePath, useNavigate } from 'react-router-dom'
import paths from 'config/paths'
import { useDispatch, useSelector } from 'react-redux'
import { actions, selectors } from '../store'
import { useEffect } from 'react'
import { LoadingStatus, UserRole } from 'shared/types'
import { selectors as loginSelectors } from 'shared/store'
import { getStatusLabel, getTodayISO, reparseISO } from 'shared/utils'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 30px;
`

const Tasks = () => {
  const columns: Column[] = [
    {
      field: 'name',
      label: 'Task name',
    },
    {
      field: 'status',
      label: 'Status',
      renderValue: value => getStatusLabel(value),
    },
    {
      field: 'worker',
      label: 'Worker name',
      renderValue: value => `${value.firstName} ${value.lastName}`,
    },
    {
      field: 'dateOpened',
      label: 'Date opened',
      renderValue: value => `${format(parseISO(value), 'dd-MM-yyyy')}`,
    },
  ]

  const { data: tasks, loading } = useSelector(selectors.getTasksSelector)
  const { data: user } = useSelector(loginSelectors.auth.loginSelector)

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.getTasks({}))
  }, []) //eslint-disable-line

  const onRowClick = (taskId: string) => {
    navigate(generatePath(paths.editTask, { taskId }))
  }

  const onFilterTable = ({
    status,
    date,
    today,
    employee,
    sortBy,
  }: TableFilterValues) => {
    dispatch(
      actions.getTasks({
        status,
        date: today ? getTodayISO() : reparseISO(date),
        ...(employee && { userId: user.id }),
        sortBy,
      })
    )
  }

  return (
    <>
      <Header>
        <PageTitle title={'Tasks'} />
      </Header>
      <TableFilter
        onSubmit={onFilterTable}
        title={'tasks'}
        loading={loading === LoadingStatus.Pending}
        role={UserRole.Worker}
        userRole={user.role}
      />
      <DataTable columns={columns} data={tasks} onRowClick={onRowClick} />
    </>
  )
}

export default Tasks
