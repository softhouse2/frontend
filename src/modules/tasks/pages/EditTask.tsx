import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { useIsUserAllowed } from 'shared/hooks'
import { FormPageContainer } from 'shared/components'
import { LoadingStatus } from 'shared/types'
import { useEditTask, TaskForm } from '../components'
import { actions, selectors } from '../store'

const EditTask = () => {
  const { editTask, isEditing } = useEditTask()
  const dispatch = useDispatch()
  const { taskId } = useParams()
  const { data: task, loading } = useSelector(selectors.getTaskSelector)

  const isAllowed = useIsUserAllowed(task?.allowedUsers)

  useEffect(() => {
    if (!taskId) return
    dispatch(actions.getTask(taskId))
  }, []) //eslint-disable-line

  return loading !== LoadingStatus.Succeeded ? null : (
    <FormPageContainer>
      <TaskForm
        onSubmit={editTask}
        loading={isEditing}
        task={task}
        title={'Edit Task'}
        allowed={isAllowed}
      />
    </FormPageContainer>
  )
}

export default EditTask
