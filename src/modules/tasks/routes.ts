import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.addTask,
    element: Loadable({
      component: () => import('./pages/CreateTask'),
    }),
  },
  {
    path: paths.editTask,
    element: Loadable({
      component: () => import('./pages/EditTask'),
    }),
  },
  {
    path: paths.tasks,
    element: Loadable({
      component: () => import('./pages/Tasks'),
    }),
  },
]

export default routes
