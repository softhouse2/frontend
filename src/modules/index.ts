import home from './home'
import members from './members'
import requests from './requests'
import tasks from './tasks'
import issues from './issues'
import clients from './clients'
import systems from './systems'

export const routes = [
  ...home.routes,
  ...members.routes,
  ...requests.routes,
  ...tasks.routes,
  ...issues.routes,
  ...clients.routes,
  ...systems.routes,
]

export const reducers = {
  home: home.reducer,
  members: members.reducer,
  requests: requests.reducer,
  issues: issues.reducer,
  tasks: tasks.reducer,
  clients: clients.reducer,
  systems: systems.reducer,
}
