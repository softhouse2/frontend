import { Formik } from 'formik'
import { Container } from 'react-bootstrap'
import { LoadingButton, TextField } from 'shared/components'
import { useAuth } from 'shared/hooks'
import {
  loginFormDefaultValues,
  LoginFormFields,
  loginFormSchema,
  LoginFormValues,
} from './LoginForm.utils'
import { StyledForm } from './LoginForm.styles'
import { LoadingStatus } from 'shared/types'

const LoginForm = () => {
  const { signIn, data } = useAuth()

  const onSubmit = ({ email, password }: LoginFormValues) => {
    signIn({ email, password })
  }

  return (
    <Formik
      initialValues={loginFormDefaultValues}
      validationSchema={loginFormSchema}
      onSubmit={onSubmit}
    >
      {() => (
        <StyledForm>
          <Container fluid className="d-flex flex-column justify-content-start">
            <TextField
              name={LoginFormFields.email}
              label={'Email address'}
              placeholder={'Enter email'}
            />
            <TextField
              name={LoginFormFields.password}
              label={'Password'}
              placeholder={'Enter password'}
              type="password"
            />
            <LoadingButton
              loading={data.loading === LoadingStatus.Pending}
              variant="primary"
              type="submit"
              style={{ marginTop: '10px' }}
            >
              Submit
            </LoadingButton>
          </Container>
        </StyledForm>
      )}
    </Formik>
  )
}
export default LoginForm
