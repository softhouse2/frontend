import { Form } from 'formik'
import styled from 'styled-components'

export const StyledForm = styled(Form)`
  margin-top: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`

export const ErrorContainer = styled.div`
  color: #ff6565;
  padding: 0.5rem 0.2em;
  height: 1.8em;
  font-size: 0.8em;
`
