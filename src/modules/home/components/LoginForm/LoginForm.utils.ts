import { object, SchemaOf, string } from 'yup'

export enum LoginFormFields {
  email = 'email',
  password = 'password',
}

export interface LoginFormValues {
  [LoginFormFields.email]: string
  [LoginFormFields.password]: string
}

export const loginFormDefaultValues: LoginFormValues = {
  [LoginFormFields.email]: '',
  [LoginFormFields.password]: '',
}

export const loginFormSchema = (): SchemaOf<LoginFormValues> =>
  object()
    .shape({
      [LoginFormFields.email]: string()
        .required('Login is required')
        .email('Must be a valid email address'),
      [LoginFormFields.password]: string().required('Password is required'),
    })
    .required()
