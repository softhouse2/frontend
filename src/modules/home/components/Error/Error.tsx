import { useEffect, useState } from 'react'
import { useAuth } from 'shared/hooks'

import { CloseButton, ErrorContainer } from './Error.styles'

const Error = () => {
  const { data } = useAuth()
  const [isHidden, setIsHidden] = useState(false)

  useEffect(() => {
    setIsHidden(false)
  }, [data.error])

  if (!data.error || isHidden) return null

  return (
    <ErrorContainer>
      <p>Incorrect email address or password</p>
      <CloseButton
        onClick={() => {
          setIsHidden(true)
        }}
      />
    </ErrorContainer>
  )
}

export default Error
