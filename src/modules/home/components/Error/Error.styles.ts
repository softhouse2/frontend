import styled from 'styled-components'
import { X } from 'react-bootstrap-icons'

export const ErrorContainer = styled.div`
  margin-top: 10px;
  width: 100%;
  height: 50px;
  border-radius: 10px;
  border: solid 1px #dcc0c4;
  background: #fedce0;
  color: #852f3d;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px;

  p {
    margin: 0;
  }
`

export const CloseButton = styled(X)`
  padding-top: 3px;
  font-size: 1.5rem;
  &:hover {
    cursor: pointer;
    transform: scale(1.2);
  }
`
