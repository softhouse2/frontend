import paths from 'config/paths'
import { useSelector } from 'react-redux'
import { selectors } from 'shared/store'
import { UserRole } from 'shared/types'

export const useInitialPage = () => {
  const { data } = useSelector(selectors.auth.loginSelector)

  if (!data) return ''

  switch (data.role) {
    case UserRole.Admin:
      return paths.members
    case UserRole.Worker:
      return paths.tasks
    case UserRole.AccountManager:
      return paths.requests
    case UserRole.ProductManager:
      return paths.issues
  }
}
