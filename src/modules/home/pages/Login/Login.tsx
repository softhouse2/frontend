import { Navigate } from 'react-router-dom'
import { useAuth } from 'shared/hooks'
import { LoginContainer } from '../../components'
import { Wrapper } from '../Login.styles'
import { useInitialPage } from './Login.utils'

const Login = () => {
  const { isAuthenticated } = useAuth()
  const initalPage = useInitialPage()

  if (isAuthenticated) return <Navigate to={initalPage} />
  return (
    <Wrapper>
      <LoginContainer title="Login" />
    </Wrapper>
  )
}

export default Login
