import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.login,
    element: Loadable({
      component: () => import('./pages/Login/Login'),
    }),
  },
]

export default routes
