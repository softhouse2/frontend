import { Field, Formik } from 'formik'
import { useEffect } from 'react'
import { Card } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { LoadingButton, StyledForm, TextField } from 'shared/components'
import Select from 'shared/components/Select'
import { actions, selectors } from 'shared/store'
import { Client, SystemVersion } from 'shared/types'
import { getSelectOptionsFromSystems } from 'shared/utils'
import {
  mapSystemVersionToValues,
  systemVersionFormDefaultValues,
  SystemVersionFormFields,
  systemVersionFormSchema,
  SystemVersionFormValues,
} from './SystemVersionForm.utils'

interface SystemVersionFormProps {
  onSubmit: (systemVersion: SystemVersionFormValues) => void
  loading: boolean
  systemVersion?: SystemVersion
  title: string
  client: Client
}

const SystemVersionForm = ({
  onSubmit,
  loading,
  systemVersion,
  title,
  client,
}: SystemVersionFormProps) => {
  const dispatch = useDispatch()
  const { data: systems } = useSelector(selectors.systems.getSystemsSelector)
  useEffect(() => {
    dispatch(actions.systems.getSystems({}))
  }, []) //eslint-disable-line

  const selectSystemOptions = getSelectOptionsFromSystems(systems)
  const initialValues = systemVersion
    ? mapSystemVersionToValues(systemVersion)
    : systemVersionFormDefaultValues

  initialValues[SystemVersionFormFields.ClientId] = client.id.toString()

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={systemVersionFormSchema}
      onSubmit={onSubmit}
    >
      <StyledForm>
        <Card className="p3">
          <Card.Body>
            <Card.Title>{title}</Card.Title>
            <p className="fs-4">
              Client: <b>{client.name}</b>
            </p>
            <Field
              type="hidden"
              name={SystemVersionFormFields.ClientId}
            ></Field>

            <Select
              name={SystemVersionFormFields.SystemId}
              label="System"
              options={selectSystemOptions}
            ></Select>
            <TextField
              name={SystemVersionFormFields.Version}
              label="Version"
            ></TextField>
            <LoadingButton
              variant="secondary"
              size="lg"
              type="submit"
              loading={loading}
            >
              Save
            </LoadingButton>
          </Card.Body>
        </Card>
      </StyledForm>
    </Formik>
  )
}

export default SystemVersionForm
