import paths from 'config/paths'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate, useParams } from 'react-router-dom'
import { CreateSystemVersionParams } from 'shared/services/clients'
import { LoadingStatus, SystemVersion } from 'shared/types'
import { number, object, SchemaOf, string } from 'yup'
import { actions, selectors } from '../../store'

export enum SystemVersionFormFields {
  ClientId = 'clientId',
  SystemId = 'systemId',
  Version = 'version',
}

export type SystemVersionFormValues = CreateSystemVersionParams

export const systemVersionFormDefaultValues: SystemVersionFormValues = {
  [SystemVersionFormFields.ClientId]: '',
  [SystemVersionFormFields.SystemId]: '0',
  [SystemVersionFormFields.Version]: '',
}

export const mapSystemVersionToValues = (systemVersion: SystemVersion) => {
  return {
    [SystemVersionFormFields.ClientId]: systemVersion.client.id.toString(),
    [SystemVersionFormFields.SystemId]: systemVersion.system.id.toString(),
    [SystemVersionFormFields.Version]: systemVersion.version,
  }
}

export const systemVersionFormSchema = (): SchemaOf<SystemVersionFormValues> =>
  object()
    .shape({
      [SystemVersionFormFields.ClientId]: number()
        .positive()
        .integer()
        .required(),
      [SystemVersionFormFields.SystemId]: number()
        .positive('Please select a system')
        .integer()
        .required(),
      [SystemVersionFormFields.Version]: string()
        .min(1, 'Version should be at least 1 character long')
        .max(8, 'Version cannot be longer than 8 characters')
        .required(),
    })
    .required()

export const useCreateSystemVersion = (clientId: string) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const createSystemVersion = (systemVersion: SystemVersionFormValues) => {
    dispatch(actions.createSystemVersion(systemVersion))
  }

  const { loading } = useSelector(selectors.createSystemVersionSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(generatePath(paths.editClient, { clientId }), {
        replace: true,
      })
    return () => {
      dispatch(actions.resetCreateSystemVersion())
    }
  }, [dispatch, loading, navigate]) //eslint-disable-line

  return {
    createSystemVersion,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditSystemVersion = (clientId: string) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { systemVersionId } = useParams()
  const editSystemVersion = (systemVersion: SystemVersionFormValues) => {
    if (!systemVersionId) return
    const parsedId = parseInt(systemVersionId)
    dispatch(actions.editSystemVersion({ id: parsedId, ...systemVersion }))
  }

  const { loading } = useSelector(selectors.editSystemVersionSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(generatePath(paths.editClient, { clientId }), { replace: true })
    return () => {
      dispatch(actions.resetEditSystemVersion())
    }
  }, [dispatch, loading, navigate]) //eslint-disable-line

  return {
    editSystemVersion,
    isEditing: loading === LoadingStatus.Pending,
  }
}
