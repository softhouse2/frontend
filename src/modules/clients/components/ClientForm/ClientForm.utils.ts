import paths from 'config/paths'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { Client, LoadingStatus } from 'shared/types'
import { object, SchemaOf, string } from 'yup'
import { actions, selectors } from '../../store'

export enum ClientFormFields {
  Name = 'name',
}

export type ClientFormValues = Omit<Client, 'id'>

export const clientFormDefaultValues: ClientFormValues = {
  [ClientFormFields.Name]: '',
}

export const clientFormSchema = (): SchemaOf<ClientFormValues> =>
  object()
    .shape({
      [ClientFormFields.Name]: string()
        .min(3, 'Name must have at least 3 characers')
        .max(40, 'Name cannot be longer than 40 characters')
        .required(),
    })
    .required()

export const useCreateClient = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const createClient = (client: ClientFormValues) => {
    dispatch(actions.createClient(client))
  }

  const { loading } = useSelector(selectors.createClientSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.clients, { replace: true })
    return () => {
      dispatch(actions.resetCreateClient())
    }
  }, [dispatch, loading, navigate])

  return {
    createClient,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditClient = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { clientId } = useParams()
  const editClient = (client: ClientFormValues) => {
    if (!clientId) return
    const parsedId = parseInt(clientId)
    dispatch(actions.editClient({ id: parsedId, ...client }))
  }

  const { loading } = useSelector(selectors.editClientSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.clients, { replace: true })
    return () => {
      dispatch(actions.resetEditClient())
    }
  }, [dispatch, loading, navigate])

  return {
    editClient,
    isEditing: loading === LoadingStatus.Pending,
  }
}
