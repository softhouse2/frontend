import { Formik } from 'formik'
import { Card } from 'react-bootstrap'
import { LoadingButton, StyledForm, TextField } from 'shared/components'
import { Client } from 'shared/types'
import {
  clientFormDefaultValues,
  ClientFormFields,
  clientFormSchema,
  ClientFormValues,
} from './ClientForm.utils'

interface ClientFormProps {
  onSubmit: (client: ClientFormValues) => void
  loading: boolean
  client?: Client
  title: string
}

const ClientForm = ({ onSubmit, loading, client, title }: ClientFormProps) => {
  return (
    <Formik
      initialValues={client ? client : clientFormDefaultValues}
      validationSchema={clientFormSchema}
      onSubmit={onSubmit}
    >
      <StyledForm>
        <Card className="p3">
          <Card.Body>
            <Card.Title>{title}</Card.Title>
            <TextField
              name={ClientFormFields.Name}
              label="Client name"
            ></TextField>
            <LoadingButton
              variant="secondary"
              size="lg"
              type="submit"
              loading={loading}
            >
              Save
            </LoadingButton>
          </Card.Body>
        </Card>
      </StyledForm>
    </Formik>
  )
}

export default ClientForm
