import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.createClient,
    element: Loadable({
      component: () => import('./pages/CreateClient'),
    }),
  },
  {
    path: paths.clients,
    element: Loadable({
      component: () => import('./pages/Clients'),
    }),
  },
  {
    path: paths.editClient,
    element: Loadable({
      component: () => import('./pages/EditClient'),
    }),
  },

  {
    path: paths.createSystemVersion,
    element: Loadable({
      component: () => import('./pages/CreateSystemVersion'),
    }),
  },
  {
    path: paths.editSystemVersion,
    element: Loadable({
      component: () => import('./pages/EditSystemVersion'),
    }),
  },
]

export default routes
