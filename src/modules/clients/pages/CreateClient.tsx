import { FormPageContainer } from 'shared/components'
import { ClientForm, useCreateClient } from '../components'

const CreateClient = () => {
  const { createClient, isCreating } = useCreateClient()
  return (
    <FormPageContainer>
      <ClientForm
        onSubmit={createClient}
        loading={isCreating}
        title={'Create Client'}
      />
    </FormPageContainer>
  )
}

export default CreateClient
