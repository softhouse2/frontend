import paths from 'config/paths'
import { useEffect } from 'react'
import { Button } from 'react-bootstrap'
import { PersonPlus } from 'react-bootstrap-icons'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate } from 'react-router-dom'
import { PageTitle } from 'shared/components'
import { DataTable, Column } from 'shared/components/DataTable'
import { actions, selectors } from 'shared/store'
import styled from 'styled-components'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 30px;
`
const Clients = () => {
  const columns: Column[] = [{ field: 'name', label: 'Name' }]

  const { data } = useSelector(selectors.clients.getClientsSelector)

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.clients.getClients({}))
  }, []) //eslint-disable-line

  const onRowClick = (clientId: string) => {
    navigate(generatePath(paths.editClient, { clientId }))
  }

  return (
    <>
      <Header>
        <PageTitle title="Clients" />
        <Button
          type="button"
          onClick={() => navigate(paths.createClient)}
          variant="outline-primary"
        >
          <PersonPlus size="30px" className="me-3" />
          Create Client
        </Button>
      </Header>
      <DataTable columns={columns} data={data} onRowClick={onRowClick} />
    </>
  )
}

export default Clients
