import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { FormPageContainer } from 'shared/components'
import { LoadingStatus } from 'shared/types'
import { SystemVersionForm, useEditSystemVersion } from '../components'
import { actions, selectors } from '../store'

const CreateSystemVersion = () => {
  const dispatch = useDispatch()
  const { clientId, systemVersionId } = useParams()
  const { data: client, loading: clientLoading } = useSelector(
    selectors.getClientSelector
  )
  const { data: systemVersion, loading: systemVersionLoading } = useSelector(
    selectors.getSystemVersionSelector
  )

  useEffect(() => {
    if (!clientId) return
    dispatch(actions.getClient(clientId))
    dispatch(actions.getSystemVersion(systemVersionId!))
  }, []) //eslint-disable-line

  const { editSystemVersion, isEditing } = useEditSystemVersion(clientId!)

  const loadingFailed =
    clientLoading !== LoadingStatus.Succeeded ||
    systemVersionLoading !== LoadingStatus.Succeeded

  return loadingFailed ? null : (
    <FormPageContainer>
      <SystemVersionForm
        onSubmit={editSystemVersion}
        loading={isEditing}
        title="Add system version"
        client={client}
        systemVersion={systemVersion}
      />
    </FormPageContainer>
  )
}

export default CreateSystemVersion
