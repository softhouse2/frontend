import { actions, selectors } from 'modules/clients/store'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate, useParams } from 'react-router-dom'
import { Column, DataTable, FormPageContainer } from 'shared/components'
import { LoadingStatus, SystemVersion } from 'shared/types'
import { ClientForm, useEditClient } from '../components'
import {
  selectors as sharedSelectors,
  actions as sharedActions,
} from 'shared/store'
import paths from 'config/paths'
import { Button } from 'react-bootstrap'
import { Header } from './Clients'
import { PlusCircle } from 'react-bootstrap-icons'

const mapSystemVersionsToTable = (systemVersions: SystemVersion[]) => {
  return systemVersions.map(({ id, system, version }) => {
    return { id: id, name: system.name, version: version }
  })
}

const EditClient = () => {
  const columns: Column[] = [
    { field: 'name', label: 'SystemName' },
    { field: 'version', label: 'Version' },
  ]

  const { editClient, isEditing } = useEditClient()
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { clientId } = useParams()
  const { data: client, loading: clientLoading } = useSelector(
    selectors.getClientSelector
  )
  const { data: systemVersions, loading: systemVersionsLoading } = useSelector(
    sharedSelectors.clients.getSystemVersionsSelector
  )

  useEffect(() => {
    if (!clientId) return
    dispatch(actions.getClient(clientId))
    dispatch(sharedActions.clients.getSystemVersions({ clientId }))
  }, []) //eslint-disable-line

  const onRowClick = (systemVersionId: string) => {
    navigate(
      generatePath(paths.editSystemVersion, {
        clientId,
        systemVersionId,
      })
    )
  }

  const loadingFailed =
    clientLoading !== LoadingStatus.Succeeded ||
    systemVersionsLoading !== LoadingStatus.Succeeded

  return loadingFailed ? null : (
    <FormPageContainer>
      <ClientForm
        onSubmit={editClient}
        loading={isEditing}
        client={client}
        title="Edit Client"
      />

      <Header>
        <h3 className="col-auto">This client's systems</h3>
        <Button
          type="button"
          onClick={() =>
            navigate(generatePath(paths.createSystemVersion, { clientId }))
          }
          variant="outline-primary"
        >
          <PlusCircle size="30px" className="me-3" />
          Add System
        </Button>
      </Header>
      <DataTable
        columns={columns}
        data={mapSystemVersionsToTable(systemVersions)}
        onRowClick={onRowClick}
      ></DataTable>
    </FormPageContainer>
  )
}

export default EditClient
