import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { FormPageContainer } from 'shared/components'
import { LoadingStatus } from 'shared/types'
import { SystemVersionForm, useCreateSystemVersion } from '../components'
import { actions, selectors } from '../store'

const CreateSystemVersion = () => {
  const dispatch = useDispatch()
  const { clientId } = useParams()
  const { data: client, loading } = useSelector(selectors.getClientSelector)

  useEffect(() => {
    if (!clientId) return
    dispatch(actions.getClient(clientId))
  }, []) //eslint-disable-line

  const { createSystemVersion, isCreating } = useCreateSystemVersion(clientId!)

  return loading !== LoadingStatus.Succeeded ? null : (
    <FormPageContainer>
      <SystemVersionForm
        onSubmit={createSystemVersion}
        loading={isCreating}
        title="Add system version"
        client={client}
      />
    </FormPageContainer>
  )
}

export default CreateSystemVersion
