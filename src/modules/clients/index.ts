import { Module } from 'shared/types'
import { reducer } from './store'
import routes from './routes'

const MODULE_NAME = 'CLIENTS'

const moduleConfig: Module<typeof reducer> = {
  name: MODULE_NAME,
  reducer,
  routes,
}

export default moduleConfig
