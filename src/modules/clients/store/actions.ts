import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import Clients, {
  CreateClientParams,
  CreateSystemVersionParams,
  EditSystemVersionParams,
} from 'shared/services/clients'
import { Client } from 'shared/types'

const clients = new Clients()

export const getClient = createAsyncThunk('getClient', (clientId: string) =>
  clients.getClient(clientId)
)

export const createClient = createAsyncThunk(
  'createClient',
  (payload: CreateClientParams) => clients.createClient(payload)
)

export const editClient = createAsyncThunk('editClient', (payload: Client) =>
  clients.editClient(payload)
)

export const getSystemVersion = createAsyncThunk(
  'getSystemVersion',
  (systemVersionId: string) => clients.getSystemVersion(systemVersionId)
)

export const createSystemVersion = createAsyncThunk(
  'createSystemVersion',
  (payload: CreateSystemVersionParams) => clients.createSystemVersion(payload)
)

export const editSystemVersion = createAsyncThunk(
  'editSystemVersion',
  (payload: EditSystemVersionParams) => clients.editSystemVersion(payload)
)

export const resetCreateClient = createAction('resetCreateClient')
export const resetEditClient = createAction('resetEditClient')

export const resetCreateSystemVersion = createAction('resetCreateSystemVersion')
export const resetEditSystemVersion = createAction('resetEditSystemVersion')
