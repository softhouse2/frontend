import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Client, SystemVersion } from 'shared/types'
import {
  createClient,
  createSystemVersion,
  editClient,
  editSystemVersion,
  getClient,
  getSystemVersion,
  resetCreateClient,
  resetEditClient,
  resetCreateSystemVersion,
  resetEditSystemVersion,
} from './actions'

export interface State {
  getClient: StateObject<Client>
  createClient: StateObject<Client>
  editClient: StateObject<Client>

  getSystemVersion: StateObject<SystemVersion>
  createSystemVersion: StateObject<SystemVersion>
  editSystemVersion: StateObject<SystemVersion>
}

const initialState: State = {
  getClient: stateObject.getInitial(),
  createClient: stateObject.getInitial(),
  editClient: stateObject.getInitial(),

  getSystemVersion: stateObject.getInitial(),
  createSystemVersion: stateObject.getInitial(),
  editSystemVersion: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getClient.pending, state => {
      stateObject.setPending(state.getClient)
    })
    .addCase(getClient.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getClient, action.payload)
    })
    .addCase(getClient.rejected, (state, action) => {
      stateObject.setFailed(state.getClient, action.error.message)
    })
    .addCase(createClient.pending, state => {
      stateObject.setPending(state.createClient)
    })
    .addCase(createClient.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createClient, action.payload)
    })
    .addCase(createClient.rejected, (state, action) => {
      stateObject.setFailed(state.createClient, action.error.message)
    })
    .addCase(resetCreateClient, state => {
      stateObject.reset(state.createClient, initialState.createClient.data)
    })
    .addCase(editClient.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editClient, action.payload)
    })
    .addCase(editClient.rejected, (state, action) => {
      stateObject.setFailed(state.editClient, action.error.message)
    })
    .addCase(resetEditClient, state => {
      stateObject.reset(state.editClient, initialState.editClient.data)
    })

    .addCase(getSystemVersion.pending, state => {
      stateObject.setPending(state.getSystemVersion)
    })
    .addCase(getSystemVersion.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getSystemVersion, action.payload)
    })
    .addCase(getSystemVersion.rejected, (state, action) => {
      stateObject.setFailed(state.getSystemVersion, action.error.message)
    })
    .addCase(createSystemVersion.pending, state => {
      stateObject.setPending(state.createSystemVersion)
    })
    .addCase(createSystemVersion.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createSystemVersion, action.payload)
    })
    .addCase(createSystemVersion.rejected, (state, action) => {
      stateObject.setFailed(state.createSystemVersion, action.error.message)
    })
    .addCase(resetCreateSystemVersion, state => {
      stateObject.reset(
        state.createSystemVersion,
        initialState.createSystemVersion.data
      )
    })
    .addCase(editSystemVersion.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editSystemVersion, action.payload)
    })
    .addCase(editSystemVersion.rejected, (state, action) => {
      stateObject.setFailed(state.editSystemVersion, action.error.message)
    })
    .addCase(resetEditSystemVersion, state => {
      stateObject.reset(
        state.editSystemVersion,
        initialState.editSystemVersion.data
      )
    })
)
