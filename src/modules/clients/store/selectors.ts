import { RootState } from 'app'

export const getClientSelector = (state: RootState) => state.clients.getClient

export const createClientSelector = (state: RootState) =>
  state.clients.createClient

export const editClientSelector = (state: RootState) => state.clients.editClient

export const getSystemVersionSelector = (state: RootState) =>
  state.clients.getSystemVersion

export const createSystemVersionSelector = (state: RootState) =>
  state.clients.createSystemVersion

export const editSystemVersionSelector = (state: RootState) =>
  state.clients.editSystemVersion
