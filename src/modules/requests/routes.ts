import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.createRequest,
    element: Loadable({
      component: () => import('./pages/CreateRequest'),
    }),
  },
  {
    path: paths.editRequest,
    element: Loadable({
      component: () => import('./pages/EditRequest'),
    }),
  },
  {
    path: paths.requests,
    element: Loadable({
      component: () => import('./pages/Requests'),
    }),
  },
]

export default routes
