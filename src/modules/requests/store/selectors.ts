import { RootState } from 'app'

export const getRequestSelector = (state: RootState) =>
  state.requests.getRequest

export const createRequestSelector = (state: RootState) =>
  state.requests.createRequest

export const editRequestSelector = (state: RootState) =>
  state.requests.editRequest
