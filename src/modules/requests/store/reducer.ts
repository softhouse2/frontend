import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Request } from 'shared/types'
import {
  createRequest,
  editRequest,
  getRequest,
  resetEditRequest,
  resetCreateRequest,
} from './actions'

export interface State {
  getRequest: StateObject<Request>
  createRequest: StateObject<Request>
  editRequest: StateObject<Request>
}

const initialState: State = {
  getRequest: stateObject.getInitial(),
  createRequest: stateObject.getInitial(),
  editRequest: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getRequest.pending, state => {
      stateObject.setPending(state.getRequest)
    })
    .addCase(getRequest.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getRequest, action.payload)
    })
    .addCase(getRequest.rejected, (state, action) => {
      stateObject.setFailed(state.getRequest, action.error.message)
    })
    .addCase(createRequest.pending, state => {
      stateObject.setPending(state.createRequest)
    })
    .addCase(createRequest.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createRequest, action.payload)
    })
    .addCase(createRequest.rejected, (state, action) => {
      stateObject.setFailed(state.createRequest, action.error.message)
    })
    .addCase(resetCreateRequest, state => {
      stateObject.reset(state.createRequest, initialState.createRequest.data)
    })
    .addCase(editRequest.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editRequest, action.payload)
    })
    .addCase(editRequest.rejected, (state, action) => {
      stateObject.setFailed(state.editRequest, action.error.message)
    })
    .addCase(resetEditRequest, state => {
      stateObject.reset(state.editRequest, initialState.editRequest.data)
    })
)
