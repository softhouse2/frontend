import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import Requests, {
  CreateRequestParams,
  EditRequestParams,
} from 'shared/services/requests'

const requests = new Requests()

export const getRequest = createAsyncThunk('getRequest', (requestId: string) =>
  requests.getRequest(requestId)
)

export const createRequest = createAsyncThunk(
  'createRequest',
  (payload: CreateRequestParams) => requests.createRequest(payload)
)

export const editRequest = createAsyncThunk(
  'editRequest',
  (payload: EditRequestParams) => requests.editRequest(payload)
)

export const resetCreateRequest = createAction('resetCreateRequest')
export const resetEditRequest = createAction('resetEditRequest')
