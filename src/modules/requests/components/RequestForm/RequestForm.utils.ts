import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import { number, object, SchemaOf, string } from 'yup'
import {
  LoadingStatus,
  Option,
  SystemVersion,
  WorkPriority,
  WorkStatus,
  Request,
} from 'shared/types'
import { actions, selectors } from '../../store'
import { useEffect } from 'react'
import paths from 'config/paths'
import { parseISO } from 'date-fns'
import { formatDate } from 'shared/utils'

export enum RequestFormFields {
  RequestName = 'name',
  Priority = 'priority',
  Status = 'status',
  Result = 'result',
  ClientId = 'clientId',
  AccountManagerId = 'accountManagerId',
  SystemId = 'systemId',
  SystemVersionId = 'systemVersionId',
  Description = 'description',
  Deadline = 'deadline',
  DateOpened = 'dateOpened',
  DateInProgress = 'dateInProgress',
  DateClosed = 'dateClosed',
}

export interface RequestFormValues {
  [RequestFormFields.RequestName]: string
  [RequestFormFields.Priority]: WorkPriority
  [RequestFormFields.Status]: WorkStatus
  [RequestFormFields.Result]: string
  [RequestFormFields.ClientId]: string
  [RequestFormFields.AccountManagerId]: string
  [RequestFormFields.SystemId]: string
  [RequestFormFields.SystemVersionId]: string
  [RequestFormFields.Description]: string
  [RequestFormFields.Deadline]: string
  [RequestFormFields.DateOpened]: string
  [RequestFormFields.DateInProgress]: string
  [RequestFormFields.DateClosed]: string
}

export const requestFormDefaultValues: RequestFormValues = {
  [RequestFormFields.RequestName]: '',
  [RequestFormFields.Priority]: WorkPriority.NORMAL,
  [RequestFormFields.Status]: WorkStatus.Opened,
  [RequestFormFields.Result]: '',
  [RequestFormFields.ClientId]: '0',
  [RequestFormFields.AccountManagerId]: '0',
  [RequestFormFields.SystemId]: '0',
  [RequestFormFields.SystemVersionId]: '0',
  [RequestFormFields.Description]: '',
  [RequestFormFields.Deadline]: '',
  [RequestFormFields.DateOpened]: formatDate(new Date().toISOString()),
  [RequestFormFields.DateInProgress]: '',
  [RequestFormFields.DateClosed]: '',
}

export const mapRequestToValues = (request: Request) => {
  return {
    [RequestFormFields.RequestName]: request.name,
    [RequestFormFields.Priority]: request.priority,
    [RequestFormFields.Status]: request.status,
    [RequestFormFields.Result]: request.result,
    [RequestFormFields.ClientId]: request.systemVersion.client.id.toString(),
    [RequestFormFields.AccountManagerId]: request.accountManager.id.toString(),
    [RequestFormFields.SystemId]: request.systemVersion.system.id.toString(),
    [RequestFormFields.SystemVersionId]: request.systemVersion.id.toString(),
    [RequestFormFields.Description]: request.description,
    [RequestFormFields.Deadline]: formatDate(request.deadline),
    [RequestFormFields.DateOpened]: formatDate(request.dateOpened),
    [RequestFormFields.DateInProgress]: formatDate(request.dateInProgress),
    [RequestFormFields.DateClosed]: formatDate(request.dateClosed),
  }
}

export const requestFormSchema = (): SchemaOf<RequestFormValues> =>
  object()
    .shape({
      [RequestFormFields.Result]: string()
        .min(3, 'Result should have at least 3 characters')
        .max(512, 'Result should not be longer than 512 characters'),
      [RequestFormFields.RequestName]: string()
        .min(3, 'Name should have at least 3 characters')
        .max(128, 'Name should not be longer than 128 characters')
        .required('Name is required'),
      [RequestFormFields.ClientId]: number()
        .integer()
        .positive('Choose a client')
        .required('Client is required'),
      [RequestFormFields.SystemId]: number()
        .integer()
        .positive('Choose a system')
        .required('System is required'),
      [RequestFormFields.SystemVersionId]: number()
        .integer()
        .positive('Choose a system version')
        .required('System version is required'),
      [RequestFormFields.Description]: string()
        .min(3, 'Description should have at least 3 characters')
        .max(1024, 'Description should not be longer than 1024 characters')
        .required('Description is required.'),
      [RequestFormFields.AccountManagerId]: string().required('Required.'),
    })
    .required()

export const useCreateRequest = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const createRequest = (request: RequestFormValues) => {
    dispatch(
      actions.createRequest({
        ...request,
        dateOpened: request.dateOpened
          ? parseISO(request.dateOpened).toISOString()
          : '',
        dateInProgress: request.dateInProgress
          ? parseISO(request.dateInProgress).toISOString()
          : '',
        dateClosed: request.dateClosed
          ? parseISO(request.dateClosed).toISOString()
          : '',
        deadline: request.deadline
          ? parseISO(request.deadline).toISOString()
          : '',
      })
    )
  }

  const { loading } = useSelector(selectors.createRequestSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.requests, { replace: true })
  }, [dispatch, loading, navigate])

  useEffect(() => {
    return () => {
      dispatch(actions.resetCreateRequest())
    }
  }, []) //eslint-disable-line

  return {
    createRequest,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditRequest = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { requestId } = useParams()
  const editRequest = (request: RequestFormValues) => {
    if (!requestId) return
    const parsedId = parseInt(requestId)
    dispatch(
      actions.editRequest({
        id: parsedId,
        ...request,
        dateOpened: request.dateOpened
          ? parseISO(request.dateOpened).toISOString()
          : '',
        dateInProgress: request.dateInProgress
          ? parseISO(request.dateInProgress).toISOString()
          : '',
        dateClosed: request.dateClosed
          ? parseISO(request.dateClosed).toISOString()
          : '',
        deadline: request.deadline
          ? parseISO(request.deadline).toISOString()
          : '',
      })
    )
  }

  const { loading } = useSelector(selectors.editRequestSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded)
      navigate(paths.requests, { replace: true })
  }, [dispatch, loading, navigate])

  useEffect(() => {
    return () => {
      dispatch(actions.resetEditRequest())
    }
  }, []) //eslint-disable-line

  return {
    editRequest,
    isEditing: loading === LoadingStatus.Pending,
  }
}

export const getSelectSystemOptionsFromVersions = (
  systemVersions: SystemVersion[] | null = null
) => {
  const retArr: Option[] = []
  systemVersions?.forEach(sv => {
    if (!retArr.find(opt => opt.value === sv.system.id)) {
      retArr.push({ value: sv.system.id, label: sv.system.name })
    }
  })

  return retArr
}

export const getSelectVersionOptionsFromSystemId = (
  systemId: string = '-1',
  systemVersions: SystemVersion[] = []
) => {
  const parsedId = parseInt(systemId)

  const retArr: Option[] = []
  systemVersions.forEach(sv => {
    if (sv.system.id === parsedId) {
      retArr.push({ value: sv.id, label: sv.version })
    }
  })

  return retArr
}
