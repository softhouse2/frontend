import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Card, Row, Col } from 'react-bootstrap'
import { Formik } from 'formik'
import { StyledForm } from 'shared/components'
import { actions, selectors } from 'shared/store'
import {
  RequestFormFields,
  RequestFormValues,
  requestFormDefaultValues,
  requestFormSchema,
  mapRequestToValues,
} from './RequestForm.utils'
import { LoadingButton, TextField } from 'shared/components'
import Select from 'shared/components/Select'
import {
  Request,
  selectStatusOptions,
  selectPriorityOptions,
  UserRole,
} from 'shared/types'
import { getSelectOptionsFromMembers } from 'shared/utils'
import DatesFields from './DatesFields'
import SystemVersionFields from './SystemVersionFields'

interface RequestFormProps {
  onSubmit: (member: RequestFormValues) => void
  loading: boolean
  request?: Request
  title: string
  allowed?: boolean
}

const RequestForm = ({
  onSubmit,
  loading,
  request,
  title,
  allowed = true,
}: RequestFormProps) => {
  const dispatch = useDispatch()

  const { data: accountManagers } = useSelector(
    selectors.members.getMembersSelector
  )
  useEffect(() => {
    dispatch(actions.members.getMembers({ role: UserRole.AccountManager }))
  }, []) //eslint-disable-line

  const selectAccountManagerOptions =
    getSelectOptionsFromMembers(accountManagers)

  const initialValues = request
    ? mapRequestToValues(request)
    : requestFormDefaultValues

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={requestFormSchema}
      onSubmit={onSubmit}
    >
      {() => (
        <StyledForm>
          <Card className=" p-3">
            <Card.Body>
              <Card.Title>{title}</Card.Title>
              <Row className="mb-3">
                <Col>
                  <TextField
                    name={RequestFormFields.RequestName}
                    type="text"
                    label="Name"
                    placeholder="Enter name"
                  />
                </Col>
                <Col>
                  <Select
                    name={RequestFormFields.Priority}
                    label="Priority"
                    options={selectPriorityOptions}
                  />
                </Col>
                <Col>
                  <Select
                    name={RequestFormFields.Status}
                    label="Status"
                    options={selectStatusOptions}
                  />
                </Col>
              </Row>
              <Row className="mb-3">
                <Col>
                  <Select
                    name={RequestFormFields.AccountManagerId}
                    placeholder="Select account manager"
                    label="Account manager"
                    options={selectAccountManagerOptions}
                    pickFirstOption={!request}
                  />
                </Col>
              </Row>
              <SystemVersionFields request={request} />
              <Card.Title>Request info</Card.Title>
              <Row className="mb-3">
                <Col>
                  <TextField
                    name={RequestFormFields.Description}
                    type="text"
                    placeholder="Enter description"
                    label="Description"
                    as="textarea"
                  />
                  <TextField
                    name={RequestFormFields.Result}
                    type="text"
                    placeholder="Enter result"
                    label="Result"
                  />
                </Col>
              </Row>
              <DatesFields request={request} />
              <LoadingButton
                variant="secondary"
                size="lg"
                type="submit"
                loading={loading}
                disabled={!allowed}
              >
                Save
              </LoadingButton>
            </Card.Body>
          </Card>
        </StyledForm>
      )}
    </Formik>
  )
}

export default RequestForm
