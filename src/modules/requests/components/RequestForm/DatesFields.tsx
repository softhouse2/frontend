import { addDays } from 'date-fns'
import { useFormikContext } from 'formik'
import { useEffect } from 'react'
import { Col, Row } from 'react-bootstrap'
import { TextField } from 'shared/components'
import { Request, WorkPriority, WorkStatus } from 'shared/types'
import { formatDate } from 'shared/utils'
import { RequestFormFields, RequestFormValues } from './RequestForm.utils'

interface DatesFieldsProps {
  request?: Request
}

const DatesFields = ({ request }: DatesFieldsProps) => {
  const { values, setFieldValue } = useFormikContext()

  const { priority, status } = values as RequestFormValues

  useEffect(() => {
    if (priority === request?.priority) {
      setFieldValue(RequestFormFields.Deadline, formatDate(request.deadline))
      return
    }
    switch (priority) {
      case WorkPriority.LOW:
        setFieldValue(
          RequestFormFields.Deadline,
          formatDate(addDays(new Date(), 14).toISOString())
        )
        break
      case WorkPriority.NORMAL:
        setFieldValue(
          RequestFormFields.Deadline,
          formatDate(addDays(new Date(), 7).toISOString())
        )
        break
      case WorkPriority.HIGH:
        setFieldValue(
          RequestFormFields.Deadline,
          formatDate(addDays(new Date(), 3).toISOString())
        )
        break
    }
  }, [priority]) //eslint-disable-line

  useEffect(() => {
    if (status === request?.status || status === WorkStatus.Opened) {
      setFieldValue(
        RequestFormFields.DateInProgress,
        request ? formatDate(request.dateInProgress) : ''
      )
      setFieldValue(
        RequestFormFields.DateClosed,
        request ? formatDate(request.dateClosed) : ''
      )
      return
    }
    switch (status) {
      case WorkStatus.InProgress:
        setFieldValue(
          RequestFormFields.DateInProgress,
          formatDate(new Date().toISOString())
        )
        setFieldValue(
          RequestFormFields.DateClosed,
          formatDate(request?.dateClosed) || ''
        )
        break
      case WorkStatus.Finished:
      case WorkStatus.Canceled:
        setFieldValue(
          RequestFormFields.DateClosed,
          formatDate(new Date().toISOString())
        )
        setFieldValue(
          RequestFormFields.DateInProgress,
          formatDate(request?.dateInProgress) || ''
        )
        break
    }
  }, [status]) //eslint-disable-line

  return (
    <>
      <Row className="mb-3">
        <Col>
          <TextField
            name={RequestFormFields.DateOpened}
            type="date"
            placeholder="Enter date opened"
            label="Open date"
            step="1"
            readOnly
          />
        </Col>
        <Col>
          <TextField
            name={RequestFormFields.Deadline}
            type="date"
            placeholder="Enter deadline"
            label="Deadline"
            step="1"
            readOnly
          />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <TextField
            name={RequestFormFields.DateInProgress}
            type="date"
            placeholder="Enter date in progress"
            label="Date in progress"
            step="1"
            readOnly
          />
        </Col>
        <Col>
          <TextField
            name={RequestFormFields.DateClosed}
            type="date"
            placeholder="Enter date finished"
            label="Date finished"
            step="1"
            readOnly
          />
        </Col>
      </Row>
    </>
  )
}

export default DatesFields
