import { useFormikContext } from 'formik'
import { useEffect, useMemo } from 'react'
import { Col, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Select from 'shared/components/Select'
import { actions, selectors } from 'shared/store'
import { Request } from 'shared/types'
import { getSelectOptionsFromClients } from 'shared/utils'
import {
  getSelectSystemOptionsFromVersions,
  getSelectVersionOptionsFromSystemId,
  RequestFormFields,
  RequestFormValues,
} from './RequestForm.utils'

export interface SystemVersionFieldsProps {
  request?: Request
}

const SystemVersionFields = ({ request }: SystemVersionFieldsProps) => {
  const dispatch = useDispatch()

  const { values, dirty } = useFormikContext<RequestFormValues>()
  const { clientId, systemId } = values as RequestFormValues

  useEffect(() => {
    dispatch(actions.clients.getClients({}))
  }, []) //eslint-disable-line

  const { data: clients } = useSelector(selectors.clients.getClientsSelector)
  const { data: systemVersions } = useSelector(
    selectors.clients.getSystemVersionsSelector
  )

  useEffect(() => {
    dispatch(
      actions.clients.getSystemVersions({ clientId: clientId.toString() })
    )
  }, [clientId]) //eslint-disable-line

  const selectClientOptions = useMemo(
    () => getSelectOptionsFromClients(clients),
    [clients]
  )

  const selectSystemOptions = useMemo(
    () => getSelectSystemOptionsFromVersions(systemVersions),
    [systemVersions] //eslint-disable-line
  )

  const selectVersionOptions = useMemo(
    () => getSelectVersionOptionsFromSystemId(systemId, systemVersions),
    [systemId, systemVersions] //eslint-disable-line
  )

  return (
    <Row className="mb-3">
      <Col>
        <Select
          name={RequestFormFields.ClientId}
          label="Client"
          options={selectClientOptions}
          pickFirstOption={!request || dirty}
        />
      </Col>
      <Col>
        <Select
          name={RequestFormFields.SystemId}
          label="System"
          options={selectSystemOptions}
          pickFirstOption={!request || dirty}
          disabled={!clientId}
        />
      </Col>
      <Col>
        <Select
          name={RequestFormFields.SystemVersionId}
          label="Version"
          options={selectVersionOptions}
          pickFirstOption={!request || dirty}
          disabled={!systemId}
        />
      </Col>
    </Row>
  )
}

export default SystemVersionFields
