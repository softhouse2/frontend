import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { FormPageContainer, IssueTable } from 'shared/components'
import { useIsUserAllowed } from 'shared/hooks'
import { LoadingStatus, WorkStatus } from 'shared/types'
import { useEditRequest, RequestForm } from '../components'
import { actions, selectors } from '../store'

const EditRequest = () => {
  const { editRequest, isEditing } = useEditRequest()
  const dispatch = useDispatch()
  const { requestId } = useParams()
  const { data: request, loading } = useSelector(selectors.getRequestSelector)

  useEffect(() => {
    if (!requestId) return
    dispatch(actions.getRequest(requestId))
  }, []) //eslint-disable-line

  const isAllowed = useIsUserAllowed(request?.allowedUsers)

  return loading !== LoadingStatus.Succeeded ? null : (
    <FormPageContainer>
      <RequestForm
        onSubmit={editRequest}
        loading={isEditing}
        request={request}
        title={'Edit request'}
        allowed={isAllowed}
      />
      {isAllowed && <IssueTable requestId={requestId} disableAdding={request.status === WorkStatus.Canceled} />}
    </FormPageContainer>
  )
}

export default EditRequest
