import {
  PageTitle,
  Column,
  DataTable,
  TableFilter,
  TableFilterValues,
} from 'shared/components'
import styled from 'styled-components'
import { generatePath, useNavigate } from 'react-router-dom'
import paths from 'config/paths'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { actions, selectors } from 'shared/store'
import { format, parseISO } from 'date-fns'
import { LoadingStatus, UserRole } from 'shared/types'
import { getStatusLabel, getTodayISO, reparseISO } from 'shared/utils'
import { Button } from 'react-bootstrap'
import { PlusCircle } from 'react-bootstrap-icons'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 20px 2%;
`

const Requests = () => {
  const columns: Column[] = [
    {
      field: 'name',
      label: 'Request name',
    },
    {
      field: 'status',
      label: 'Status',
      renderValue: value => getStatusLabel(value),
    },
    {
      field: 'accountManager',
      label: 'Account manager name',
      renderValue: value => `${value.firstName} ${value.lastName}`,
    },
    {
      field: 'dateOpened',
      label: 'Date opened',
      renderValue: value => `${format(parseISO(value), 'dd-MM-yyyy')}`,
    },
  ]

  const { data: requests, loading } = useSelector(
    selectors.requests.getRequestsSelector
  )
  const { data: user } = useSelector(selectors.auth.loginSelector)

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.requests.getRequests({}))
  }, []) //eslint-disable-line

  const onRowClick = (requestId: string) => {
    navigate(generatePath(paths.editRequest, { requestId }))
  }

  const onFilterTable = ({
    status,
    date,
    today,
    employee,
    sortBy,
  }: TableFilterValues) => {
    dispatch(
      actions.requests.getRequests({
        status,
        date: today ? getTodayISO() : reparseISO(date),
        ...(employee && { userId: user.id }),
        sortBy,
      })
    )
  }

  return (
    <>
      <Header>
        <PageTitle title={'Requests'} />
        {user && user.role === UserRole.Admin && (
          <Button
            type="button"
            onClick={() => navigate(paths.createRequest)}
            variant="outline-primary"
          >
            <PlusCircle size="30px" className="me-3" />
            Create request
          </Button>
        )}
      </Header>
      <TableFilter
        onSubmit={onFilterTable}
        title={'requests'}
        loading={loading === LoadingStatus.Pending}
        role={UserRole.AccountManager}
        userRole={user.role}
      />
      <DataTable columns={columns} data={requests} onRowClick={onRowClick} />
    </>
  )
}

export default Requests
