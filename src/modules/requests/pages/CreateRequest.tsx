import { FormPageContainer } from 'shared/components'
import { useCreateRequest, RequestForm } from '../components'

const CreateRequest = () => {
  const { createRequest, isCreating } = useCreateRequest()

  return (
    <FormPageContainer>
      <RequestForm
        onSubmit={createRequest}
        loading={isCreating}
        title={'Create request'}
      />
    </FormPageContainer>
  )
}

export default CreateRequest
