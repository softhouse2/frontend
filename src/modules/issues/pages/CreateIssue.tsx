import { useCreateIssue } from '../components/IssueForm'
import IssueForm from '../components/IssueForm'
import { FormPageContainer } from 'shared/components'

const CreateIssue = () => {
  const { createIssue, isCreating } = useCreateIssue()
  return (
    <FormPageContainer>
      <IssueForm
        onSubmit={createIssue}
        loading={isCreating}
        title={'Create issue'}
      />
    </FormPageContainer>
  )
}

export default CreateIssue
