import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router-dom'
import { FormPageContainer, TaskTable } from 'shared/components'
import { useIsUserAllowed } from 'shared/hooks'
import { LoadingStatus, WorkStatus } from 'shared/types'
import { useEditIssue, IssueForm } from '../components'
import { actions, selectors } from '../store'

const EditIssue = () => {
  const { editIssue, isEditing } = useEditIssue()
  const dispatch = useDispatch()
  const { issueId } = useParams()
  const { data: issue, loading } = useSelector(selectors.getIssueSelector)

  const isAllowed = useIsUserAllowed(issue?.allowedUsers)

  useEffect(() => {
    if (!issueId) return
    dispatch(actions.getIssue(issueId))
  }, []) //eslint-disable-line

  return loading !== LoadingStatus.Succeeded ? null : (
    <FormPageContainer>
      <IssueForm
        onSubmit={editIssue}
        loading={isEditing}
        issue={issue}
        title={'Edit issue'}
        allowed={isAllowed}
      />
      {isAllowed && <TaskTable issueId={issueId} disableAdding={issue.status === WorkStatus.Canceled}/>}
    </FormPageContainer>
  )
}

export default EditIssue
