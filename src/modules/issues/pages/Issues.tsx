import { format, parseISO } from 'date-fns'
import {
  PageTitle,
  DataTable,
  TableFilter,
  Column,
  TableFilterValues,
} from 'shared/components'
import styled from 'styled-components'
import { generatePath, useNavigate } from 'react-router-dom'
import paths from 'config/paths'
import { useDispatch, useSelector } from 'react-redux'
import { actions, selectors } from 'shared/store'
import { useEffect } from 'react'
import { LoadingStatus, UserRole } from 'shared/types'
import { getStatusLabel, getTodayISO, reparseISO } from 'shared/utils'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 10px 30px;
`

const Issues = () => {
  const columns: Column[] = [
    {
      field: 'name',
      label: 'Issue name',
    },
    {
      field: 'status',
      label: 'Status',
      renderValue: value => getStatusLabel(value),
    },
    {
      field: 'productManager',
      label: 'Product manager name',
      renderValue: value => `${value.firstName} ${value.lastName}`,
    },
    {
      field: 'dateOpened',
      label: 'Date opened',
      renderValue: value => `${format(parseISO(value), 'dd-MM-yyyy')}`,
    },
  ]

  const { data: issues, loading } = useSelector(
    selectors.issues.getIssuesSelector
  )
  const { data: user } = useSelector(selectors.auth.loginSelector)

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.issues.getIssues({}))
  }, []) //eslint-disable-line

  const onRowClick = (issueId: string) => {
    navigate(generatePath(paths.editIssue, { issueId }))
  }

  const onFilterTable = ({
    status,
    date,
    today,
    employee,
    sortBy,
  }: TableFilterValues) => {
    dispatch(
      actions.issues.getIssues({
        status,
        date: today ? getTodayISO() : reparseISO(date),
        ...(employee && { userId: user.id }),
        sortBy,
      })
    )
  }

  return (
    <>
      <Header>
        <PageTitle title={'Issues'} />
      </Header>
      <TableFilter
        onSubmit={onFilterTable}
        title={'issues'}
        loading={loading === LoadingStatus.Pending}
        role={UserRole.ProductManager}
        userRole={user.role}
      />
      <DataTable columns={columns} data={issues} onRowClick={onRowClick} />
    </>
  )
}

export default Issues
