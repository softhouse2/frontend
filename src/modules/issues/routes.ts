import paths from 'config/paths'
import { Loadable } from 'shared/components'
import { ModalRoute } from 'shared/types'

const routes: ModalRoute[] = [
  {
    path: paths.addIssue,
    element: Loadable({
      component: () => import('./pages/CreateIssue'),
    }),
  },
  {
    path: paths.issues,
    element: Loadable({
      component: () => import('./pages/Issues'),
    }),
  },
  {
    path: paths.editIssue,
    element: Loadable({
      component: () => import('./pages/EditIssue'),
    }),
  },
]

export default routes
