import { Card, Row, Col } from 'react-bootstrap'
import { Formik } from 'formik'
import {
  IssueFormFields,
  IssueFormValues,
  issueFormDefaultValues,
  issueFormSchema,
} from './IssueForm.utils'
import { LoadingButton, StyledForm, TextField } from 'shared/components'
import Select from 'shared/components/Select'
import {
  selectIssueTypeOptions,
  selectPriorityOptions,
  selectStatusOptions,
  Issue,
  UserRole,
} from 'shared/types'
import { useDispatch, useSelector } from 'react-redux'
import { actions, selectors } from 'shared/store'
import { useEffect } from 'react'
import { formatDate, getSelectOptionsFromMembers } from 'shared/utils'
import DatesFields from './DatesFields'

interface IssueFormProps {
  onSubmit: (issue: IssueFormValues) => void
  loading: boolean
  issue?: Issue
  title: string
  allowed?: boolean
}

const IssueForm = ({
  onSubmit,
  loading,
  issue,
  title,
  allowed = true,
}: IssueFormProps) => {
  const dispatch = useDispatch()
  const { data: productManagers } = useSelector(
    selectors.members.getMembersSelector
  )

  useEffect(() => {
    dispatch(actions.members.getMembers({ role: UserRole.ProductManager }))
  }, []) //eslint-disable-line

  const selectProductManagerOptions =
    getSelectOptionsFromMembers(productManagers)

  const initialValues = issue
    ? {
        ...issue,
        dateOpened: formatDate(issue.dateOpened),
        deadline: formatDate(issue.deadline),
        dateClosed: formatDate(issue.dateClosed),
        dateInProgress: formatDate(issue.dateInProgress),
        productManagerId: issue.productManager.id.toString(),
      }
    : issueFormDefaultValues

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={issueFormSchema}
      onSubmit={onSubmit}
    >
      {() => {
        return (
          <StyledForm>
            <Card className="p-3">
              <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Row className="mb-3">
                  <Col>
                    <Select
                      name={IssueFormFields.Type}
                      label="Type"
                      options={selectIssueTypeOptions}
                    />
                  </Col>
                  <Col>
                    <Select
                      name={IssueFormFields.Priority}
                      label="Priority"
                      options={selectPriorityOptions}
                    />
                  </Col>
                  <Col>
                    <Select
                      name={IssueFormFields.Status}
                      label="Status"
                      options={selectStatusOptions}
                    />
                  </Col>
                </Row>
                <Row className="mb-3">
                  <Col>
                    <TextField
                      name={IssueFormFields.Name}
                      type="text"
                      placeholder="Enter issue's name"
                      label="Issue's name"
                    />
                  </Col>
                  <Col>
                    <Select
                      name={IssueFormFields.ProductManagerId}
                      label="Product manager's name"
                      options={selectProductManagerOptions}
                      pickFirstOption={!issue}
                    />
                  </Col>
                </Row>
                <Card.Title>Issue info</Card.Title>
                <Row className="mb-3">
                  <Col>
                    <TextField
                      name={IssueFormFields.Description}
                      label="Description"
                      as="textarea"
                    />
                    <TextField
                      name={IssueFormFields.Result}
                      label="Result"
                      as="textarea"
                    />
                  </Col>
                </Row>
                <DatesFields issue={issue} />
                <LoadingButton
                  variant="secondary"
                  size="lg"
                  type="submit"
                  loading={loading}
                  disabled={!allowed}
                >
                  Save
                </LoadingButton>
              </Card.Body>
            </Card>
          </StyledForm>
        )
      }}
    </Formik>
  )
}

export default IssueForm
