import { IssueType, WorkPriority, WorkStatus } from 'shared/types'
import { object, SchemaOf, string } from 'yup'
import paths from 'config/paths'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate, useParams } from 'react-router-dom'
import { LoadingStatus } from 'shared/types'
import { actions, selectors } from '../../store'
import { parseISO } from 'date-fns'
import { formatDate } from 'shared/utils'

export enum IssueFormFields {
  Type = 'type',
  Priority = 'priority',
  Status = 'status',
  Result = 'result',
  Name = 'name',
  ProductManagerId = 'productManagerId',
  Description = 'description',
  Deadline = 'deadline',
  DateOpened = 'dateOpened',
  DateInProgress = 'dateInProgress',
  DateClosed = 'dateClosed',
  RequestId = 'requestId',
}

export interface IssueFormValues {
  [IssueFormFields.Type]: IssueType
  [IssueFormFields.Priority]: WorkPriority
  [IssueFormFields.Status]: WorkStatus
  [IssueFormFields.Result]: string
  [IssueFormFields.Name]: string
  [IssueFormFields.ProductManagerId]: string
  [IssueFormFields.Deadline]: string
  [IssueFormFields.DateOpened]: string
  [IssueFormFields.DateInProgress]: string
  [IssueFormFields.DateClosed]: string
  [IssueFormFields.RequestId]: number
  [IssueFormFields.Description]: string
}

export const issueFormDefaultValues: IssueFormValues = {
  [IssueFormFields.Type]: IssueType.Feature,
  [IssueFormFields.Priority]: WorkPriority.NORMAL,
  [IssueFormFields.Status]: WorkStatus.Opened,
  [IssueFormFields.Result]: '',
  [IssueFormFields.Name]: '',
  [IssueFormFields.ProductManagerId]: '',
  [IssueFormFields.Description]: '',
  [IssueFormFields.Deadline]: '',
  [IssueFormFields.DateOpened]: formatDate(new Date().toISOString()),
  [IssueFormFields.DateInProgress]: '',
  [IssueFormFields.DateClosed]: '',
  [IssueFormFields.RequestId]: 0,
}

export const issueFormSchema = (): SchemaOf<IssueFormValues> =>
  object()
    .shape({
      [IssueFormFields.Name]: string()
        .min(3, 'Name should have at least 3 characters')
        .max(128, 'Name should not be longer than 128 characters')
        .required('Name is required'),
      [IssueFormFields.Description]: string()
        .min(3, 'Description should have at least 3 characters')
        .max(512, 'Description should be no longer than 512 characters')
        .required('Description is required.'),
      [IssueFormFields.Result]: string()
        .min(3, 'Result should have at least 3 characters')
        .max(512, 'Result should be no longer than 512 characters'),
      [IssueFormFields.ProductManagerId]: string().required('Required.'),
    })
    .required()

export const useCreateIssue = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { requestId } = useParams()
  const createIssue = (issue: IssueFormValues) => {
    if (!requestId) return
    dispatch(
      actions.createIssue({
        ...issue,
        requestId: parseInt(requestId, 10),
        dateOpened: issue.dateOpened
          ? parseISO(issue.dateOpened).toISOString()
          : '',
        dateInProgress: issue.dateInProgress
          ? parseISO(issue.dateInProgress).toISOString()
          : '',
        dateClosed: issue.dateClosed
          ? parseISO(issue.dateClosed).toISOString()
          : '',
        deadline: issue.deadline ? parseISO(issue.deadline).toISOString() : '',
        productManagerId: Number(issue.productManagerId),
      })
    )
  }

  const { loading } = useSelector(selectors.createIssueSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded) {
      navigate(generatePath(paths.editRequest, { requestId }), {
        replace: true,
      })
    }
  }, [dispatch, loading, navigate, requestId])

  useEffect(() => {
    return () => {
      dispatch(actions.resetCreateIssue())
    }
  }, []) //eslint-disable-line

  return {
    createIssue,
    isCreating: loading === LoadingStatus.Pending,
  }
}

export const useEditIssue = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { issueId } = useParams()
  const editIssue = (issue: IssueFormValues) => {
    if (!issueId) return
    dispatch(
      actions.editIssue({
        id: issueId as unknown as number,
        ...issue,
        dateOpened: issue.dateOpened
          ? parseISO(issue.dateOpened).toISOString()
          : '',
        dateInProgress: issue.dateInProgress
          ? parseISO(issue.dateInProgress).toISOString()
          : '',
        dateClosed: issue.dateClosed
          ? parseISO(issue.dateClosed).toISOString()
          : '',
        deadline: issue.deadline ? parseISO(issue.deadline).toISOString() : '',
        productManagerId: Number(issue.productManagerId),
      })
    )
  }

  const { loading } = useSelector(selectors.editIssueSelector)

  useEffect(() => {
    if (loading === LoadingStatus.Succeeded) navigate(-1)
  }, [dispatch, loading, navigate])

  useEffect(() => {
    return () => {
      dispatch(actions.resetEditIssue())
    }
  }, []) //eslint-disable-line

  return {
    editIssue,
    isEditing: loading === LoadingStatus.Pending,
  }
}
