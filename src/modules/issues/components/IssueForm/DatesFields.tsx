import { addDays } from 'date-fns'
import { useFormikContext } from 'formik'
import { useEffect } from 'react'
import { Col, Row } from 'react-bootstrap'
import { TextField } from 'shared/components'
import { Issue, WorkPriority, WorkStatus } from 'shared/types'
import { formatDate } from 'shared/utils'
import { IssueFormFields, IssueFormValues } from './IssueForm.utils'

interface DatesFieldsProps {
  issue?: Issue
}

const DatesFields = ({ issue }: DatesFieldsProps) => {
  const { values, setFieldValue } = useFormikContext()

  const { priority, status } = values as IssueFormValues

  useEffect(() => {
    if (priority === issue?.priority) {
      setFieldValue(IssueFormFields.Deadline, formatDate(issue.deadline))
      return
    }
    switch (priority) {
      case WorkPriority.LOW:
        setFieldValue(
          IssueFormFields.Deadline,
          formatDate(addDays(new Date(), 14).toISOString())
        )
        break
      case WorkPriority.NORMAL:
        setFieldValue(
          IssueFormFields.Deadline,
          formatDate(addDays(new Date(), 7).toISOString())
        )
        break
      case WorkPriority.HIGH:
        setFieldValue(
          IssueFormFields.Deadline,
          formatDate(addDays(new Date(), 3).toISOString())
        )
        break
    }
  }, [priority]) //eslint-disable-line

  useEffect(() => {
    if (status === issue?.status || status === WorkStatus.Opened) {
      setFieldValue(
        IssueFormFields.DateInProgress,
        issue ? formatDate(issue.dateInProgress) : ''
      )
      setFieldValue(
        IssueFormFields.DateClosed,
        issue ? formatDate(issue.dateClosed) : ''
      )
      return
    }
    switch (status) {
      case WorkStatus.InProgress:
        setFieldValue(
          IssueFormFields.DateInProgress,
          formatDate(new Date().toISOString())
        )
        setFieldValue(
          IssueFormFields.DateClosed,
          formatDate(issue?.dateClosed) || ''
        )
        break
      case WorkStatus.Finished:
      case WorkStatus.Canceled:
        setFieldValue(
          IssueFormFields.DateClosed,
          formatDate(new Date().toISOString())
        )
        setFieldValue(
          IssueFormFields.DateInProgress,
          formatDate(issue?.dateInProgress) || ''
        )
        break
    }
  }, [status]) //eslint-disable-line

  return (
    <>
      <Row className="mb-3">
        <Col>
          <TextField
            name={IssueFormFields.DateOpened}
            type="date"
            placeholder="Enter date opened"
            label="Open date"
            step="1"
            readOnly
          />
        </Col>
        <Col>
          <TextField
            name={IssueFormFields.Deadline}
            type="date"
            placeholder="Enter deadline"
            label="Deadline"
            step="1"
            readOnly
          />
        </Col>
      </Row>
      <Row className="mb-3">
        <Col>
          <TextField
            name={IssueFormFields.DateInProgress}
            type="date"
            placeholder="Enter date in progress"
            label="Date in progress"
            step="1"
            readOnly
          />
        </Col>
        <Col>
          <TextField
            name={IssueFormFields.DateClosed}
            type="date"
            placeholder="Enter date finished"
            label="Date finished"
            step="1"
            readOnly
          />
        </Col>
      </Row>
    </>
  )
}

export default DatesFields
