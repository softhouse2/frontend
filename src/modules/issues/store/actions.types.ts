import { Issue } from 'shared/types'

export interface EditIssuePayload extends Issue {
  onSuccess: () => {}
}
