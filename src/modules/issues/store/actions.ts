import { createAction, createAsyncThunk } from '@reduxjs/toolkit'
import Issues, {
  CreateIssueParams,
  EditIssueParams,
} from 'shared/services/issues'

const issues = new Issues()

export const getIssue = createAsyncThunk('getIssue', (issueId: string) =>
  issues.getIssue(issueId)
)

export const createIssue = createAsyncThunk(
  'createIssue',
  (payload: CreateIssueParams) => issues.createIssue(payload)
)

export const editIssue = createAsyncThunk(
  'editIssue',
  (payload: EditIssueParams) => issues.editIssue(payload)
)

export const resetCreateIssue = createAction('resetCreateIssue')
export const resetEditIssue = createAction('resetEditIssue')
