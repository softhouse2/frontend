import { RootState } from 'app'

export const getIssueSelector = (state: RootState) => state.issues.getIssue

export const createIssueSelector = (state: RootState) =>
  state.issues.createIssue

export const editIssueSelector = (state: RootState) => state.issues.editIssue
