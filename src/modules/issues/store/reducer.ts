import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Issue } from 'shared/types'
import {
  createIssue,
  editIssue,
  getIssue,
  resetCreateIssue,
  resetEditIssue,
} from './actions'

export interface State {
  getIssue: StateObject<Issue>
  createIssue: StateObject<Issue>
  editIssue: StateObject<Issue>
}

const initialState: State = {
  getIssue: stateObject.getInitial(),
  createIssue: stateObject.getInitial(),
  editIssue: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getIssue.pending, state => {
      stateObject.setPending(state.getIssue)
    })
    .addCase(getIssue.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getIssue, action.payload)
    })
    .addCase(getIssue.rejected, (state, action) => {
      stateObject.setFailed(state.getIssue, action.error.message)
    })
    .addCase(createIssue.pending, state => {
      stateObject.setPending(state.createIssue)
    })
    .addCase(createIssue.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.createIssue, action.payload)
    })
    .addCase(createIssue.rejected, (state, action) => {
      stateObject.setFailed(state.createIssue, action.error.message)
    })
    .addCase(resetCreateIssue, state => {
      stateObject.reset(state.createIssue, initialState.createIssue.data)
    })
    .addCase(editIssue.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.editIssue, action.payload)
    })
    .addCase(editIssue.rejected, (state, action) => {
      stateObject.setFailed(state.editIssue, action.error.message)
    })
    .addCase(resetEditIssue, state => {
      stateObject.reset(state.editIssue, initialState.editIssue.data)
    })
)
