import { Client, SystemVersion } from 'shared/types'
import Api from '../api'
import {
  CreateClientParams,
  CreateSystemVersionParams,
  EditSystemVersionParams,
  GetClientsParams,
  GetSystemVersionsParams,
} from './Clients.types'

class Clients extends Api {
  private static URL = '/clients'
  private static SV_URL = '/systemVersions'

  public getClients = async (params: GetClientsParams) => {
    const { data } = await this.api.get<Client[]>(Clients.URL, { params })
    return data
  }

  public getClient = async (clientId: string) => {
    const { data } = await this.api.get<Client>(`${Clients.URL}/${clientId}`)
    return data
  }

  public createClient = async (payload: CreateClientParams) => {
    const { data } = await this.api.post<Client>(Clients.URL, payload)
    return data
  }

  public editClient = async ({ id, ...payload }: Client) => {
    const { data } = await this.api.put<Client>(`${Clients.URL}/${id}`, payload)
    return data
  }

  public createSystemVersion = async (payload: CreateSystemVersionParams) => {
    const { data } = await this.api.post<SystemVersion>(
      `${Clients.SV_URL}`,
      payload
    )
    return data
  }

  public editSystemVersion = async ({
    id,
    ...payload
  }: EditSystemVersionParams) => {
    const { data } = await this.api.put<SystemVersion>(
      `${Clients.SV_URL}/${id}`,
      payload
    )
    return data
  }

  public getSystemVersions = async (params: GetSystemVersionsParams) => {
    const { data } = await this.api.get<SystemVersion[]>(Clients.SV_URL, {
      params,
    })
    return data
  }

  public getSystemVersion = async (systemVersionId: string) => {
    const { data } = await this.api.get<SystemVersion>(
      `${Clients.SV_URL}/${systemVersionId}`
    )
    return data
  }
}

export default Clients
