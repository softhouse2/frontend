import { Client, SystemVersion } from 'shared/types'

export type CreateClientParams = Omit<Client, 'id'>

export interface GetClientsParams {
  systemId?: number
}

export interface GetSystemVersionsParams {
  systemId?: string
  clientId?: string
}

export type EditSystemVersionParams = Omit<
  SystemVersion,
  'system' | 'client'
> & {
  systemId: string
  clientId: string
}

export type CreateSystemVersionParams = Omit<EditSystemVersionParams, 'id'>
