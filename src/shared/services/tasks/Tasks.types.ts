import { SortingOption } from 'shared/components'
import { Task } from 'shared/types'

export interface EditTaskParams extends Omit<Task, 'worker' | 'allowedUsers'> {
  workerId: number
}

export interface CreateTaskParams extends Omit<EditTaskParams, 'id'> {}

export interface GetTasksParams {
  status?: any
  date?: Date | string
  userId?: number
  issueId?: string
  sortBy?: SortingOption
}
