import Api from '../api'
import { Task } from 'shared/types'
import { CreateTaskParams, EditTaskParams, GetTasksParams } from './Tasks.types'

class Tasks extends Api {
  public getTasks = async (params: GetTasksParams) => {
    const { data } = await this.api.get<Task[]>(`/tasks`, {
      params,
    })
    return data
  }

  public getTask = async (taskId: string) => {
    const { data } = await this.api.get<Task>(`/tasks/${taskId}`)
    return data
  }

  public createTask = async (payload: CreateTaskParams) => {
    const { data } = await this.api.post<Task>(`/tasks`, payload)
    return data
  }

  public editTask = async ({ id, ...payload }: EditTaskParams) => {
    const { data } = await this.api.put<Task>(`/tasks/${id}`, payload)
    return data
  }
}

export default Tasks
