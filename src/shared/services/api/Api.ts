import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
import config from 'config/constants'

class Api {
  protected api: AxiosInstance = axios.create({
    baseURL: config.api,
    withCredentials: false,
  })

  constructor() {
    this.api.interceptors.request.use(this.handleRequest)
  }

  private handleRequest = (requestConfig: AxiosRequestConfig) => {
    const token = sessionStorage.getItem('token')
    requestConfig.headers = {
      ...(token && { Authorization: `Bearer ${token}` }),
    }

    return requestConfig
  }
}

export default Api
