import Api from '../api'
import { Request } from 'shared/types'
import {
  CreateRequestParams,
  EditRequestParams,
  GetRequestsParams,
} from './Requests.types'

class Requests extends Api {
  public getRequests = async (params: GetRequestsParams) => {
    const { data } = await this.api.get<Request[]>(`/requests`, {
      params,
    })
    return data
  }

  public getRequest = async (requestId: string) => {
    const { data } = await this.api.get<Request>(`/requests/${requestId}`)
    return data
  }

  public createRequest = async (payload: CreateRequestParams) => {
    const { data } = await this.api.post<Request>(`/requests`, payload)
    return data
  }

  public editRequest = async ({ id, ...payload }: EditRequestParams) => {
    const { data } = await this.api.put<Request>(`/requests/${id}`, payload)
    return data
  }
}

export default Requests
