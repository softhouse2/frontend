import { SortingOption } from 'shared/components'
import { Request } from 'shared/types'

export interface EditRequestParams
  extends Omit<Request, 'systemVersion' | 'accountManager' | 'allowedUsers'> {
  systemVersionId: string
  accountManagerId: string
}

export interface CreateRequestParams extends Omit<EditRequestParams, 'id'> {}

export interface GetRequestsParams {
  status?: any
  date?: Date | string
  userId?: number
  sortBy?: SortingOption
}
