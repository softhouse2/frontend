import Api from '../api'
import { Issue } from 'shared/types'
import {
  CreateIssueParams,
  EditIssueParams,
  GetIssuesParams,
} from './Issues.types'

class Issues extends Api {
  public getIssues = async (params: GetIssuesParams) => {
    const { data } = await this.api.get<Issue[]>(`/issues`, {
      params,
    })
    return data
  }

  public getIssue = async (issueId: string) => {
    const { data } = await this.api.get<Issue>(`/issues/${issueId}`)
    return data
  }

  public createIssue = async (payload: CreateIssueParams) => {
    const { data } = await this.api.post<Issue>(`/issues`, payload)
    return data
  }

  public editIssue = async ({ id, ...payload }: EditIssueParams) => {
    const { data } = await this.api.put<Issue>(`/issues/${id}`, payload)
    return data
  }
}

export default Issues
