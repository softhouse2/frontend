import { SortingOption } from 'shared/components'
import { Issue } from 'shared/types'

export interface EditIssueParams
  extends Omit<Issue, 'productManager' | 'allowedUsers'> {
  productManagerId: number
}

export interface CreateIssueParams extends Omit<EditIssueParams, 'id'> {}

export interface GetIssuesParams {
  status?: any
  date?: Date | string
  userId?: number
  requestId?: string
  sortBy?: SortingOption
}
