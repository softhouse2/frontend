import { System } from 'shared/types'

export type CreateSystemParams = Omit<System, 'id'>

export interface GetSystemsParams {
  systemId?: number
  clientId?: any
}
