import { System } from 'shared/types'
import Api from '../api'
import { CreateSystemParams, GetSystemsParams } from './Systems.types'

class Systems extends Api {
  private static URL = '/systems'

  public getSystems = async (params: GetSystemsParams) => {
    const { data } = await this.api.get<System[]>(Systems.URL, { params })
    return data
  }

  public getSystem = async (systemId: string) => {
    const { data } = await this.api.get<System>(`${Systems.URL}/${systemId}`)
    return data
  }

  public createSystem = async (payload: CreateSystemParams) => {
    const { data } = await this.api.post<System>(Systems.URL, payload)
    return data
  }

  public editSystem = async ({ id, ...payload }: System) => {
    const { data } = await this.api.put<System>(`${Systems.URL}/${id}`, payload)
    return data
  }
}

export default Systems
