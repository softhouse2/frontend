import { Member } from 'shared/types'
import Api from '../api'
import { CreateMemberParams, GetMembersParams } from './Members.types'

class Members extends Api {
  public getMembers = async (params: GetMembersParams) => {
    const { data } = await this.api.get<Member[]>(`/users`, {
      params,
    })
    return data
  }

  public getMember = async (memberId: string) => {
    const { data } = await this.api.get<Member>(`/users/${memberId}`)
    return data
  }

  public createMember = async (payload: CreateMemberParams) => {
    const { data } = await this.api.post<Member>(`/users`, payload)
    return data
  }

  public editMember = async ({ id, ...payload }: Member) => {
    const { data } = await this.api.put<Member>(`/users/${id}`, payload)
    return data
  }
}

export default Members
