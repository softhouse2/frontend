import { Member, UserRole } from 'shared/types'

export type CreateMemberParams = Omit<Member, 'id'>

export interface GetMembersParams {
  role?: UserRole
}
