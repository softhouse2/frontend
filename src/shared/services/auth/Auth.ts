import Api from '../api'
import { LoginParams, LoginResponse } from './Auth.types'

class Auth extends Api {
  public login = async (payload: LoginParams) => {
    const { data } = await this.api.post<LoginResponse>(`/users/login`, payload)
    return data
  }
}

export default Auth
