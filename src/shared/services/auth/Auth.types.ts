import { Member } from 'shared/types'

export interface LoginParams {
  email: string
  password: string
}

export interface LoginResponse {
  token: string
  user: Member
}
