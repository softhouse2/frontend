import { format, parseISO } from 'date-fns'
import {
  Issue,
  Member,
  Task,
  Request,
  selectStatusOptions,
  System,
  Client,
  selectRoleOptions,
} from 'shared/types'

export const getSelectOptionsFromMembers = (members: Member[]) =>
  members
    .filter(({ active }) => active)
    .map(({ id, firstName, lastName }) => ({
      value: id,
      label: `${firstName} ${lastName}`,
    }))

export const getSelectOptionsFromWorkType = (
  workType: Request[] | Task[] | Issue[]
) =>
  workType.map(({ id, name }) => ({
    value: id,
    label: name,
  }))

export const getSelectOptionsFromSystems = (systems: System[]) => {
  const retArr = [{ value: 0, label: '- Select system -' }]

  return retArr.concat(
    systems.map(({ id, name }) => ({
      value: id,
      label: name,
    }))
  )
}

export const getSelectOptionsFromClients = (clients: Client[] = []) =>
  clients.map(({ id, name }) => ({
    value: id,
    label: name,
  }))

export const identity = <T>(value: T) => value

export const getStatusLabel = (statusValue: string) =>
  selectStatusOptions.find(({ value }) => value === statusValue)?.label

export const getRoleLabel = (roleValue: string) =>
  selectRoleOptions.find(({ value }) => value === roleValue)?.label

export const formatDate = (date?: string) =>
  date ? format(parseISO(date), 'Y-MM-dd') : ''

export const reparseISO = (date?: string) =>
  date ? parseISO(date).toISOString() : ''

export const getTodayISO = () =>
  reparseISO(formatDate(new Date().toISOString()))
