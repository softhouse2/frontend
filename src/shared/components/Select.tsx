import { Form, FormControlProps } from 'react-bootstrap'
import { Field, ErrorMessage, useField } from 'formik'
import styled from 'styled-components'
import { useEffect } from 'react'
import { Option } from '../types/data'

const ErrorContainer = styled.div`
  color: #ff6565;
  padding: 0.5rem 0.2em;
  height: 1.8em;
  font-size: 0.8em;
`
interface SelectProps extends FormControlProps {
  name: string
  label: string
  options: Option[]
  pickFirstOption?: boolean
}

const Select = ({
  name,
  label,
  options,
  pickFirstOption,
  disabled = false,
  ...props
}: SelectProps) => {
  const [field, meta, helpers] = useField(name)
  const { touched, error } = meta
  const isInvalid = (error || '').length > 0 && touched

  useEffect(() => {
    if (options.length > 0 && pickFirstOption)
      helpers.setValue(options[0].value)
  }, [options]) //eslint-disable-line

  return (
    <Field name={name}>
      {() => (
        <Form.Group className="mb-3" controlId={name}>
          <Form.Label>{label}</Form.Label>
          <Form.Select
            aria-label="Default select example"
            isInvalid={isInvalid}
            disabled={disabled}
            {...field}
          >
            {options.map(option => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </Form.Select>
          {<ErrorMessage component={ErrorContainer} name={name} />}
        </Form.Group>
      )}
    </Field>
  )
}

export default Select
