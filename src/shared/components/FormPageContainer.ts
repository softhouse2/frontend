import styled from 'styled-components'

const FormPageContainer = styled.div`
  width: 100%;
  padding-left: 20px;
  padding-right: 20px;

  form {
    margin: 20px 0;
    width: 100%;
  }

  > div {
    margin: 20px 0 0 0;
    width: 100%;
  }
`

export default FormPageContainer
