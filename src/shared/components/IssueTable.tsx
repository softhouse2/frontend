import paths from 'config/paths'
import { format, parseISO } from 'date-fns'
import { useEffect } from 'react'
import { Button, Card } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate } from 'react-router-dom'
import { DataTable, Column, StyledCard } from 'shared/components'
import { actions, selectors } from 'shared/store'
import styled from 'styled-components'
import { getStatusLabel } from 'shared/utils'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
interface IssueTableProps {
  requestId?: string
  disableAdding?: boolean
}

const IssueTable = ({ requestId, disableAdding }: IssueTableProps) => {
  const columns: Column[] = [
    {
      field: 'name',
      label: 'Issue name',
    },
    {
      field: 'status',
      label: 'Status',
      renderValue: value => getStatusLabel(value),
    },
    {
      field: 'productManager',
      label: 'Product manager name',
      renderValue: value => `${value.firstName} ${value.lastName}`,
    },
    {
      field: 'dateOpened',
      label: 'Date opened',
      renderValue: value => `${format(parseISO(value), 'dd-MM-yyyy')}`,
    },
  ]

  const { data } = useSelector(selectors.issues.getIssuesSelector)
  const navigate = useNavigate()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.issues.getIssues({ requestId }))
  }, []) //eslint-disable-line

  const onRowClick = (issueId: string) => {
    navigate(generatePath(paths.editIssue, { issueId }))
  }

  return (
    <>
      <StyledCard className=" p-3">
        <Card.Body>
          <Header>
            <h2>Issues</h2>
            <Button
              type="button"
              onClick={() =>
                navigate(generatePath(paths.addIssue, { requestId }))
              }
              variant="outline-primary"
              disabled={disableAdding}
            >
              Add Issue
            </Button>
          </Header>
          <DataTable columns={columns} data={data} onRowClick={onRowClick} />
        </Card.Body>
      </StyledCard>
    </>
  )
}

export default IssueTable
