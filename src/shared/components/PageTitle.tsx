import styled from 'styled-components'

interface PageTitleProps {
  title: string
}

const TitleContainer = styled.div`
  font-size: 3rem;
  display: inline-block;
`

const PageTitle = ({ title }: PageTitleProps) => {
  return <TitleContainer>{title}</TitleContainer>
}

export default PageTitle
