import { useField } from 'formik'
import { useEffect } from 'react'
import TextField from '../TextField'
import { TableFilterFields } from './TableFilter.utils'
interface DateFieldProps {
  disabled?: boolean
}
export const DateField = ({ disabled }: DateFieldProps) => {
  const [field, meta, helpers] = useField<string>(TableFilterFields.Date) // eslint-disable-line
  useEffect(() => {
    if (disabled) helpers.setValue('')
  }, [disabled]) // eslint-disable-line
  return (
    <TextField
      name={TableFilterFields.Date}
      type="date"
      placeholder="Date"
      label="Date"
      step="1"
      disabled={disabled}
    />
  )
}
