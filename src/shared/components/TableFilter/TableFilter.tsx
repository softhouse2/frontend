import {
  tableFilterDefaultValues,
  TableFilterFields,
  tableFilterSchema,
  TableFilterValues,
  selectStatusOptions,
  selectSortByOptions,
} from './TableFilter.utils'
import Checkbox from '../Checkbox'
import Select from '../Select'
import { StyledForm } from './TableFilter.styles'
import { Card, Row, Col } from 'react-bootstrap'
import LoadingButton from '../LoadingButton'
import { Formik } from 'formik'
import { DateField } from './TableFilter.components'
import { UserRole } from 'shared/types'

interface TableFilterProps {
  onSubmit: (filter: TableFilterValues) => void
  loading: boolean
  title: string
  role: UserRole
  userRole: UserRole
}

const TableFilter = ({
  onSubmit,
  loading,
  title,
  role,
  userRole,
}: TableFilterProps) => {
  return (
    <Formik
      initialValues={tableFilterDefaultValues}
      validationSchema={tableFilterSchema}
      onSubmit={onSubmit}
    >
      {({ values }) => {
        const isTodaySelected = values.today
        return (
          <StyledForm>
            <Card className="w-100">
              <Card.Body>
                <Row className="mb-3">
                  <Col>
                    <Select
                      name={TableFilterFields.Status}
                      label="Status"
                      options={selectStatusOptions}
                      size="sm"
                    />
                  </Col>
                  <Col>
                    <DateField disabled={isTodaySelected} />
                  </Col>
                  <Col>
                    <Select
                      name={TableFilterFields.SortBy}
                      label="Sort by"
                      options={selectSortByOptions}
                      size="sm"
                    />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Checkbox
                      name={TableFilterFields.Today}
                      label={"Display today's " + title}
                    />
                  </Col>
                  <Col>
                    <Checkbox
                      name={TableFilterFields.Employee}
                      label={'Display my ' + title}
                      disabled={role !== userRole}
                    />
                  </Col>
                </Row>

                <LoadingButton
                  variant="secondary"
                  type="submit"
                  loading={loading}
                >
                  Filter
                </LoadingButton>
              </Card.Body>
            </Card>
          </StyledForm>
        )
      }}
    </Formik>
  )
}

export default TableFilter
