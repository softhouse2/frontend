import { Form } from 'formik'
import styled from 'styled-components'

export const StyledForm = styled(Form)`
  display: flex;
  margin-left: 2%;
  margin-right: 2%;
`