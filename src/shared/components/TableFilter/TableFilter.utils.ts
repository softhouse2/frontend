import { object, SchemaOf } from 'yup'
import { Option, WorkStatus } from 'shared/types'

export enum TableFilterFields {
  Employee = 'employee',
  Date = 'date',
  Today = 'today',
  Status = 'status',
  SortBy = 'sortBy',
}

export enum SortingOption {
  None = '',
  NameAscending = 'NAME_ASC',
  NameDescending = 'NAME_DESC',
  DateAscending = 'DATE_ASC',
  DateDescending = 'DATE_DESC',
}

export interface TableFilterValues {
  [TableFilterFields.Employee]: boolean
  [TableFilterFields.Today]: boolean
  [TableFilterFields.Date]: string
  [TableFilterFields.Status]: any
  [TableFilterFields.SortBy]: SortingOption
}

export const tableFilterDefaultValues: TableFilterValues = {
  [TableFilterFields.Employee]: false,
  [TableFilterFields.Today]: false,
  [TableFilterFields.Date]: '',
  [TableFilterFields.Status]: '',
  [TableFilterFields.SortBy]: SortingOption.None,
}

export const selectStatusOptions: Option[] = [
  { value: '', label: 'Any' },
  { value: WorkStatus.Opened, label: 'Open' },
  { value: WorkStatus.InProgress, label: 'In progress' },
  { value: WorkStatus.Finished, label: 'Finished' },
  { value: WorkStatus.Canceled, label: 'Canceled' },
]

export const selectSortByOptions: Option[] = [
  { value: SortingOption.None, label: '' },
  { value: SortingOption.NameAscending, label: 'Name' },
  { value: SortingOption.NameDescending, label: 'Name, descending' },
  { value: SortingOption.DateAscending, label: 'Date' },
  { value: SortingOption.DateDescending, label: 'Date, descending' },
]

export const tableFilterSchema = (): SchemaOf<TableFilterValues> =>
  object().shape({}).required()

const toExport = {
  selectStatusOptions,
  selectSortByOptions,
  SortingOption,
}

export default toExport
