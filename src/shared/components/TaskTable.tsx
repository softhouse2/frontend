import paths from 'config/paths'
import { format, parseISO } from 'date-fns'
import { useEffect } from 'react'
import { Button, Card } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { generatePath, useNavigate } from 'react-router-dom'
import { DataTable, Column, StyledCard } from 'shared/components'
import { actions, selectors } from 'shared/store'
import styled from 'styled-components'
import { getStatusLabel } from 'shared/utils'

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
interface TaskTableProps {
  issueId?: string
  disableAdding?: boolean
}

const TaskTable = ({ issueId, disableAdding }: TaskTableProps) => {
  const columns: Column[] = [
    {
      field: 'name',
      label: 'Task name',
    },
    {
      field: 'status',
      label: 'Status',
      renderValue: value => getStatusLabel(value),
    },
    {
      field: 'worker',
      label: 'Worker name',
      renderValue: value => `${value.firstName} ${value.lastName}`,
    },
    {
      field: 'dateOpened',
      label: 'Date opened',
      renderValue: value => `${format(parseISO(value), 'dd-MM-yyyy')}`,
    },
  ]

  const { data } = useSelector(selectors.tasks.getTasksSelector)
  const navigate = useNavigate()
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(actions.tasks.getTasks({ issueId }))
  }, []) //eslint-disable-line

  const onRowClick = (taskId: string) => {
    navigate(generatePath(paths.editTask, { taskId }))
  }

  return (
    <>
      <StyledCard className=" p-3">
        <Card.Body>
          <Header>
            <h2>Tasks</h2>
            <Button
              type="button"
              onClick={() => navigate(generatePath(paths.addTask, { issueId }))}
              variant="outline-primary"
              disabled={disableAdding}
            >
              Add Task
            </Button>
          </Header>
          <DataTable columns={columns} data={data} onRowClick={onRowClick} />
        </Card.Body>
      </StyledCard>
    </>
  )
}

export default TaskTable
