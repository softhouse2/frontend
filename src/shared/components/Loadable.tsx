import { lazy, Suspense } from 'react'
import Loader from './Loader'

interface LoadableProps {
  component(): Promise<{ default: React.ComponentType<any> }>
}

const Loadable = ({ component }: LoadableProps) => {
  const Component = lazy(component)
  return (
    <Suspense fallback={<Loader />}>
      <Component />
    </Suspense>
  )
}

export default Loadable
