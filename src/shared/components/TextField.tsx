import { Form, FormControlProps } from 'react-bootstrap'
import { Field, ErrorMessage, useField } from 'formik'
import styled from 'styled-components'

const ErrorContainer = styled.div`
  color: #ff6565;
  padding: 0.5rem 0.2em;
  height: 1.8em;
  font-size: 0.8em;
`

interface TextFieldProps extends FormControlProps {
  name: string
  label: string
  step?: string
}

const TextField = ({ name, label, placeholder, step, ...props }: TextFieldProps) => {
  const [field, meta] = useField(name)
  const { touched, error } = meta
  const isInvalid = (error || '').length > 0 && touched
  return (
    <Field name={name}>
      {() => (
        <Form.Group className="mb-3" controlId={name}>
          <Form.Label>{label}</Form.Label>
          <Form.Control
            type="text"
            placeholder={placeholder}
            isInvalid={isInvalid}
            step={step}
            {...props}
            {...field}
          />
          {<ErrorMessage component={ErrorContainer} name={name} />}
        </Form.Group>
      )}
    </Field>
  )
}

export default TextField
