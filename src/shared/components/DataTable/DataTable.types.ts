import { ReactNode } from 'react'
export interface Column {
  field: string
  label: string
  renderValue?: (value: any) => ReactNode
}
