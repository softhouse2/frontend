import React from 'react'
import { identity } from 'shared/utils'
import { StyledTable } from './DataTable.styles'
import { Column } from './DataTable.types'

interface DataTableProps {
  columns: Column[]
  data: any[]
  onRowClick?: (id: string) => void
}

const DataTable = ({
  columns,
  data,
  onRowClick = () => {},
}: DataTableProps) => {
  return (
    <div className="table-container">
      <StyledTable>
        <thead>
          <tr>
            {columns.map(({ label }, i) => (
              <th key={`head-column-${i}`}>{label}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((dataObject: any, i) => (
            <tr key={`data-row-${i}`} onClick={() => onRowClick(dataObject.id)}>
              {columns.map(({ field, renderValue = identity }, index) => (
                <td key={`data-cell-${i}-${index}`}>
                  {renderValue(dataObject[field])}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </StyledTable>
    </div>
  )
}

export default DataTable
