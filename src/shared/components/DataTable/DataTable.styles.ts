import { Table } from 'react-bootstrap'
import styled from 'styled-components'

export const StyledTable = styled(Table)`
  border-spacing: 0 5px;
  border-collapse: separate;
  tbody > tr {
    background: #f0f0f0;
    &:hover {
      background: #e3e3e3;
      cursor: pointer;
    }
  }
`
