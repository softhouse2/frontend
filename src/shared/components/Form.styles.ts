import { Form } from 'formik'
import styled from 'styled-components'

const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  align-items: left;
  margin-left: 5%;
  margin-top: 2%;
`

export default StyledForm
