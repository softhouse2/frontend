import { PropsWithChildren } from 'react'
import { Button, ButtonProps, Spinner } from 'react-bootstrap'

interface LoadingButtonProps extends PropsWithChildren<ButtonProps> {
  loading: boolean
}

const LoadingButton = ({
  loading,
  children,
  variant,
  ...props
}: LoadingButtonProps) => {
  return (
    <Button
      disabled={loading}
      variant={loading ? 'secondary' : variant}
      className="d-flex align-items-center justify-content-center"
      {...props}
    >
      {loading ? (
        <>
          <Spinner
            animation="border"
            variant="primary"
            size="sm"
            style={{ marginRight: '10px' }}
          />
          {children}
        </>
      ) : (
        children
      )}
    </Button>
  )
}

export default LoadingButton
