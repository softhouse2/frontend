import { Card } from "react-bootstrap"
import styled from "styled-components"

const StyledCard = styled(Card)`
  flex-direction: column;
  align-items: left;
  margin-left: 4%;
  margin-top: 1%;
`

export default StyledCard;