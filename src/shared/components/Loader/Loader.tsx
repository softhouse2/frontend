import { Spinner } from 'react-bootstrap'
import styled from 'styled-components'

const SpinnerContainer = styled.div`
  height: 100vh;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`

const StyledSpinner = styled(Spinner)`
  height: 80px;
  width: 80px;
  border: 10px solid currentColor;
  border-right-color: transparent;
`

const Loader = () => {
  return (
    <SpinnerContainer>
      <StyledSpinner animation="border" variant="primary" />
    </SpinnerContainer>
  )
}

export default Loader
