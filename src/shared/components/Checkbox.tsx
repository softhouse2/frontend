import { Form, FormControlProps } from 'react-bootstrap'
import { Field, ErrorMessage, useField } from 'formik'
import styled from 'styled-components'

const ErrorContainer = styled.div`
  color: #ff6565;
  padding: 0.5rem 0.2em;
  height: 1.8em;
  font-size: 0.8em;
`
interface CheckboxProps extends FormControlProps {
  name: string
  label: string
  disabled?: boolean
}

const Checkbox = ({ name, label, disabled, ...props }: CheckboxProps) => {
  const [field, meta] = useField(name)
  const { touched, error } = meta
  const isInvalid = (error || '').length > 0 && touched

  return (
    <Field name={name}>
      {() => (
        <Form.Group className="mb-3" controlId={name}>
          <Form.Check
            type="checkbox"
            id={name}
            label={label}
            isInvalid={isInvalid}
            disabled={disabled}
            {...field}
          />
          {<ErrorMessage component={ErrorContainer} name={name} />}
        </Form.Group>
      )}
    </Field>
  )
}

export default Checkbox
