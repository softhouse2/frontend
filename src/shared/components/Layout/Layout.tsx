import paths from 'config/paths'
import { useEffect } from 'react'
import {
  Route,
  Navigate,
  Routes,
  useLocation,
  useNavigate,
} from 'react-router-dom'
import { useAuth } from 'shared/hooks'
import { ModalRoute } from 'shared/types'
import styled from 'styled-components'
import Drawer from './Drawer'
import Topbar from './Topbar'

interface LayoutProps {
  routes: ModalRoute[]
}

const TOPBAR_HEIGHT = 70
const DRAWER_WIDTH = 150
interface MainContentProps {
  withLayoutFix?: boolean
}

const MainContent = styled.main<MainContentProps>`
  position: absolute;
  width: 100%;

  ${({ withLayoutFix }) =>
    withLayoutFix &&
    `
  padding-top: ${TOPBAR_HEIGHT}px;
  padding-left: ${DRAWER_WIDTH + 20}px;
  padding-right: 20px;
  padding-bottom: 20px;

  > .table-container {
    margin:20px 2%;
  }

  `}
`

const Layout = ({ routes }: LayoutProps) => {
  const { isAuthenticated } = useAuth()
  const currentPath = useLocation().pathname
  const navigate = useNavigate()

  useEffect(() => {
    if (!isAuthenticated) {
      navigate(paths.login)
    }
  }, [isAuthenticated]) //eslint-disable-line

  return (
    <>
      <MainContent withLayoutFix={isAuthenticated}>
        <Routes>
          {routes.map((route, i) => (
            <Route
              key={`route-${i}`}
              path={route.path}
              element={route.element}
            />
          ))}
          <Route path="/" element={<Navigate to={paths.login} />} />
        </Routes>
      </MainContent>
      {isAuthenticated && (
        <>
          <Drawer
            width={DRAWER_WIDTH}
            paddingTop={TOPBAR_HEIGHT}
            currentPath={currentPath}
          />
          <Topbar height={TOPBAR_HEIGHT} />
        </>
      )}
    </>
  )
}

export default Layout
