import paths from 'config/paths'
import { useSelector } from 'react-redux'
import { selectors } from 'shared/store'
import { UserRole } from 'shared/types'

interface DrawerItem {
  label: string
  destination: string
}

const workerDrawerItems: DrawerItem[] = [
  {
    label: 'Tasks',
    destination: paths.tasks,
  },
]

const productManagerDrawerItems: DrawerItem[] = [
  {
    label: 'Issues',
    destination: paths.issues,
  },
  ...workerDrawerItems,
]

const accountManagerDrawerItems: DrawerItem[] = [
  {
    label: 'Requests',
    destination: paths.requests,
  },
  ...productManagerDrawerItems,
]

const adminDrawerItems: DrawerItem[] = [
  {
    label: 'Members',
    destination: paths.members,
  },
  {
    label: 'Clients',
    destination: paths.clients,
  },
  {
    label: 'Systems',
    destination: paths.systems,
  },
  ...accountManagerDrawerItems,
]

export const useDrawerItems = (): DrawerItem[] => {
  const { data } = useSelector(selectors.auth.loginSelector)

  switch (data.role) {
    case UserRole.Admin:
      return adminDrawerItems
    case UserRole.Worker:
      return workerDrawerItems
    case UserRole.AccountManager:
      return accountManagerDrawerItems
    case UserRole.ProductManager:
      return productManagerDrawerItems
  }
}
