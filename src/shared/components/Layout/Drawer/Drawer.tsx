import { Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { Container } from './Drawer.styles'
import { useDrawerItems } from './Drawer.utils'

interface DrawerProps {
  width: number
  paddingTop: number
  currentPath: string
}

const Drawer = ({ width, paddingTop, currentPath }: DrawerProps) => {
  const modulePath = `/${currentPath.split('/')[1]}`
  const drawerItems = useDrawerItems()
  return (
    <Container width={width} paddingTop={paddingTop} className="position-fixed">
      <Nav
        activeKey={modulePath}
        className="border-end border-bottom shadow flex-column h4 text-center row-gap-3"
        justify={true}
        variant="pills"
        style={{ height: '100%' }}
      >
        <div className="text-center">
          {drawerItems.map(({ label, destination }, i) => (
            <Nav.Item key={`drawer-item-${i}`}>
              <Nav.Link as={Link} to={destination} href={destination}>
                {label}
              </Nav.Link>
            </Nav.Item>
          ))}
        </div>
      </Nav>
    </Container>
  )
}

export default Drawer
