import styled from 'styled-components'

interface ContainerProps {
  width: number
  paddingTop: number
}

export const Container = styled('div')<ContainerProps>`
  ${({ width, paddingTop }) => `
    width: ${width}px;
    padding-top:${paddingTop}px;
`}
  height: 100vh;

  & .nav-link {
    border-radius: 0;
  }
`
