import { Navbar, Nav, NavDropdown } from 'react-bootstrap'
import icon from '../../../assets/logo.png'
import { useDispatch, useSelector } from 'react-redux'
import { selectors, actions } from 'shared/store'
import { StyledNavbar } from './Topbar.styles'
import { PersonCircle } from 'react-bootstrap-icons'
import paths from 'config/paths'
import { useNavigate } from 'react-router-dom'

interface TopbarProps {
  height: number
}

const Topbar = ({ height }: TopbarProps) => {
  const { data } = useSelector(selectors.auth.loginSelector)
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const logOut = () => {
    dispatch(actions.auth.logoutUser())
    navigate(paths.login)
  }
  const { firstName, lastName } = data

  return (
    <StyledNavbar height={height} bg="dark" variant="dark">
      <Navbar.Brand>
        <img src={icon} width="30" height="30" alt="logo" /> Softhouse
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse className="justify-content-end">
        <Navbar.Brand>{`${firstName} ${lastName}`}</Navbar.Brand>
        <Nav>
          <NavDropdown
            title={<PersonCircle size="30px" />}
            id="dropdown-Nav"
            align="end"
          >
            <NavDropdown.Item onClick={logOut}>Log out</NavDropdown.Item>
          </NavDropdown>
        </Nav>
      </Navbar.Collapse>
    </StyledNavbar>
  )
}
export default Topbar
