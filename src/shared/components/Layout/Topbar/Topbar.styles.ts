import { Navbar } from 'react-bootstrap'
import styled from 'styled-components'

interface NavbarProps {
  height: number
}

export const StyledNavbar = styled(Navbar)<NavbarProps>`
  ${({ height }) => `height: ${height}px;`}
  position: fixed;
  top: 0px;
  left: 0px;
  width: 100%;
  padding: 0 20px;
`
