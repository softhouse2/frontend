import { useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { LoginParams } from 'shared/services'
import { UserRole } from 'shared/types'
import {
  actions as commonActions,
  selectors as commonSelectors,
} from '../store'

export const useAuth = () => {
  const dispatch = useDispatch()
  const signIn = ({ email, password }: LoginParams) =>
    dispatch(
      commonActions.auth.loginUser({
        email,
        password,
      })
    )

  const data = useSelector(commonSelectors.auth.loginSelector)
  const isAuthenticated = useMemo(() => !!data.data, [data.data])
  return {
    data,
    isAuthenticated,
    signIn,
  }
}

export const useIsUserAllowed = (allowedIds?: number[]) => {
  const { data: user } = useSelector(commonSelectors.auth.loginSelector)
  return allowedIds
    ? user.role === UserRole.Admin || allowedIds.includes(user.id)
    : true
}
