import {
  WorkPriority,
  WorkStatus,
  TaskType,
  IssueType,
  UserRole,
  Option,
} from 'shared/types'

export const selectPriorityOptions: Option[] = [
  { value: WorkPriority.LOW, label: 'Low' },
  { value: WorkPriority.NORMAL, label: 'Normal' },
  { value: WorkPriority.HIGH, label: 'High' },
]

export const selectStatusOptions: Option[] = [
  { value: WorkStatus.Opened, label: 'Open' },
  { value: WorkStatus.InProgress, label: 'In progress' },
  { value: WorkStatus.Finished, label: 'Finished' },
  { value: WorkStatus.Canceled, label: 'Canceled' },
]

export const selectTaskTypeOptions: Option[] = [
  { value: TaskType.Analysis, label: 'Analysis' },
  { value: TaskType.Design, label: 'Design' },
  { value: TaskType.Docs, label: 'Docs' },
  { value: TaskType.Programming, label: 'Programming' },
  { value: TaskType.Testing, label: 'Testing' },
]

export const selectIssueTypeOptions: Option[] = [
  { value: IssueType.Feature, label: 'Feature' },
  { value: IssueType.Bug, label: 'Bug' },
  { value: IssueType.Improvement, label: 'Improvement' },
]

export const selectActivityStatusOptions: Option[] = [
  { value: true, label: 'Active' },
  { value: false, label: 'Inactive' },
]

export const selectRoleOptions: Option[] = [
  { value: UserRole.Admin, label: 'Admin' },
  { value: UserRole.AccountManager, label: 'Account manager' },
  { value: UserRole.ProductManager, label: 'Product manager' },
  { value: UserRole.Worker, label: 'Worker' },
]
