export enum LoadingStatus {
  Idle = 'idle',
  Pending = 'pending',
  Succeeded = 'succeeded',
  Failed = 'failed',
}

export enum UserRole {
  Admin = 'ADMIN',
  ProductManager = 'PRODUCT_MANAGER',
  AccountManager = 'ACCOUNT_MANAGER',
  Worker = 'WORKER',
}

export enum WorkPriority {
  HIGH = 'HIGH',
  NORMAL = 'NORMAL',
  LOW = 'LOW',
}

export enum WorkStatus {
  Opened = 'OPEN',
  InProgress = 'IN_PROGRESS',
  Finished = 'FINISHED',
  Canceled = 'CANCELED',
}

export enum TaskType {
  Analysis = 'ANALYSIS',
  Programming = 'PROGRAMMING',
  Testing = 'TESTING',
  Docs = 'DOCS',
  Design = 'DESIGN',
}

export enum IssueType {
  Feature = 'FEATURE',
  Bug = 'BUG',
  Improvement = 'IMPROVEMENT',
}
