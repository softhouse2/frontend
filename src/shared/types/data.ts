import {
  UserRole,
  IssueType,
  WorkPriority,
  WorkStatus,
  TaskType,
} from './enums'

export interface Member {
  id: number
  active: boolean
  firstName: string
  lastName: string
  role: UserRole
  email: string
  telNum: string
  password: string
}

export interface Issue {
  id: number
  name: string
  type: IssueType
  priority: WorkPriority
  status: WorkStatus
  result: string
  requestId: number
  productManager: Member
  description: string
  dateOpened: string
  dateInProgress: string
  dateClosed: string
  deadline: string
  allowedUsers: number[]
}

export interface Request {
  id: number
  name: string
  priority: WorkPriority
  status: WorkStatus
  result: string
  accountManager: Member
  systemVersion: SystemVersion
  description: string
  deadline: string
  dateOpened: string
  dateInProgress: string
  dateClosed: string
  allowedUsers: number[]
}

export interface Task {
  id: number
  name: string
  priority: WorkPriority
  status: WorkStatus
  result: string
  worker: Member
  type: TaskType
  issueId: number
  deadline: string
  dateOpened: string
  dateInProgress: string
  dateClosed: string
  allowedUsers: number[]
}

export interface Option {
  value: any
  label: string
}

export interface Client {
  id: number
  name: string
}

export interface System {
  id: number
  name: string
}

export interface SystemVersion {
  id: number
  client: Client
  version: string
  system: System
}
