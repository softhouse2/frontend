import { combineReducers } from 'redux'
import auth, { State as AuthState } from './auth'
import members, { State as MembersState } from './members'
import requests, { State as RequestsState } from './requests'
import issues, { State as IssuesState } from './issues'
import clients, { State as ClientsState } from './clients'
import tasks, { State as TasksState } from './tasks'
import systems, { State as SystemState } from './systems'

export interface CommonState {
  common: {
    auth: AuthState
    members: MembersState
    requests: RequestsState
    issue: IssuesState
    clients: ClientsState
    task: TasksState
    systems: SystemState
  }
}

export const actions = {
  auth: auth.actions,
  members: members.actions,
  requests: requests.actions,
  issues: issues.actions,
  clients: clients.actions,
  tasks: tasks.actions,
  systems: systems.actions,
}

export const selectors = {
  auth: auth.selectors,
  members: members.selectors,
  requests: requests.selectors,
  issues: issues.selectors,
  clients: clients.selectors,
  tasks: tasks.selectors,
  systems: systems.selectors,
}

export const reducer = combineReducers({
  auth: auth.reducer,
  members: members.reducer,
  requests: requests.reducer,
  issues: issues.reducer,
  clients: clients.reducer,
  tasks: tasks.reducer,
  systems: systems.reducer,
})
