import { createAsyncThunk } from '@reduxjs/toolkit'
import Systems, { GetSystemsParams } from 'shared/services/systems'

const systems = new Systems()

export const getSystems = createAsyncThunk(
  'getSystems',
  (params: GetSystemsParams) => systems.getSystems(params)
)
