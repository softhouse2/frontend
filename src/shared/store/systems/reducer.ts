import { createReducer } from '@reduxjs/toolkit'
import { stateObject, StateObject } from 'shared/redux'
import { System } from 'shared/types'
import { getSystems } from './actions'

export interface State {
  getSystems: StateObject<System[]>
}

const initialState: State = {
  getSystems: stateObject.getInitial([]),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getSystems.pending, state => {
      stateObject.setPending(state.getSystems)
    })
    .addCase(getSystems.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getSystems, action.payload)
    })
    .addCase(getSystems.rejected, (state, action) => {
      stateObject.setFailed(state.getSystems, action.error.message)
    })
)
