import { RootState } from 'app'

export const getSystemsSelector = (state: RootState) =>
  state.common.systems.getSystems
