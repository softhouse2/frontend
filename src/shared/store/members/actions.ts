import { createAsyncThunk } from '@reduxjs/toolkit'
import Members, { GetMembersParams } from 'shared/services/members'

const members = new Members()

export const getMembers = createAsyncThunk(
  'getMembers',
  (payload: GetMembersParams) => members.getMembers(payload)
)
