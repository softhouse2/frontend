import { RootState } from 'app'

export const getMembersSelector = (state: RootState) =>
  state.common.members.getMembers
