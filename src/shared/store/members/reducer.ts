import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Member } from 'shared/types'
import { getMembers } from './actions'

export interface State {
  getMembers: StateObject<Member[]>
}

const initialState: State = {
  getMembers: stateObject.getInitial([]),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getMembers.pending, state => {
      stateObject.setPending(state.getMembers)
    })
    .addCase(getMembers.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getMembers, action.payload)
    })
    .addCase(getMembers.rejected, (state, action) => {
      stateObject.setFailed(state.getMembers, action.error.message)
    })
)
