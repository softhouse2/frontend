import { createAsyncThunk } from '@reduxjs/toolkit'
import Request, { GetRequestsParams } from 'shared/services/requests'

const request = new Request()

export const getRequests = createAsyncThunk('getRequests', (payload: GetRequestsParams) =>
  request.getRequests(payload)
)
