import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Request } from 'shared/types'
import { getRequests } from './actions'

export interface State {
  getRequests: StateObject<Request[]>
}

const initialState: State = {
  getRequests: stateObject.getInitial([]),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getRequests.pending, state => {
      stateObject.setPending(state.getRequests)
    })
    .addCase(getRequests.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getRequests, action.payload)
    })
    .addCase(getRequests.rejected, (state, action) => {
      stateObject.setFailed(state.getRequests, action.error.message)
    })
)
