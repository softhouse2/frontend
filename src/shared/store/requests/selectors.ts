import { RootState } from 'app'

export const getRequestsSelector = (state: RootState) =>
  state.common.requests.getRequests
