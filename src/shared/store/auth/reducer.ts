import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Member } from 'shared/types'
import { loginUser, logoutUser } from './actions'

export interface State {
  loginUser: StateObject<Member>
}

const initialState: State = {
  loginUser: stateObject.getInitial(),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(loginUser.pending, state => {
      stateObject.setPending(state.loginUser)
    })
    .addCase(loginUser.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.loginUser, action.payload.user)
      sessionStorage.setItem('token', action.payload.token)
    })
    .addCase(loginUser.rejected, (state, action) => {
      stateObject.setFailed(state.loginUser, action.error.message)
    })
    .addCase(logoutUser, state => {
      state.loginUser = stateObject.getInitial()
    })
)
