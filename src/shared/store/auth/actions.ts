import { createAction, createAsyncThunk } from '@reduxjs/toolkit'

import Auth, { LoginParams } from 'shared/services/auth'

const auth = new Auth()

export const loginUser = createAsyncThunk(`loginUser`, (payload: LoginParams) =>
  auth.login(payload)
)

export const logoutUser = createAction(`logoutUser`)
