import { RootState } from 'app'

export const loginSelector = (state: RootState) => state.common.auth.loginUser
