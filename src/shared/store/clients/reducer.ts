import { createReducer } from '@reduxjs/toolkit'
import { stateObject, StateObject } from 'shared/redux'
import { Client, SystemVersion } from 'shared/types'
import { getClients, getSystemVersions } from './actions'

export interface State {
  getClients: StateObject<Client[]>
  getSystemVersions: StateObject<SystemVersion[]>
}

const initialState: State = {
  getClients: stateObject.getInitial([]),
  getSystemVersions: stateObject.getInitial([]),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getClients.pending, state => {
      stateObject.setPending(state.getClients)
    })
    .addCase(getClients.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getClients, action.payload)
    })
    .addCase(getClients.rejected, (state, action) => {
      stateObject.setFailed(state.getClients, action.error.message)
    })

    .addCase(getSystemVersions.pending, state => {
      stateObject.setPending(state.getSystemVersions)
    })
    .addCase(getSystemVersions.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getSystemVersions, action.payload)
    })
    .addCase(getSystemVersions.rejected, (state, action) => {
      stateObject.setFailed(state.getSystemVersions, action.error.message)
    })
)
