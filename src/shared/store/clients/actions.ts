import { createAsyncThunk } from '@reduxjs/toolkit'
import Clients, {
  GetClientsParams,
  GetSystemVersionsParams,
} from 'shared/services/clients'

const clients = new Clients()

export const getClients = createAsyncThunk(
  'getClients',
  (params: GetClientsParams) => clients.getClients(params)
)

export const getSystemVersions = createAsyncThunk(
  'getSystemVersions',
  (params: GetSystemVersionsParams) => clients.getSystemVersions(params)
)
