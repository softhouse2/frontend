import { RootState } from 'app'

export const getClientsSelector = (state: RootState) =>
  state.common.clients.getClients

export const getSystemVersionsSelector = (state: RootState) =>
  state.common.clients.getSystemVersions
