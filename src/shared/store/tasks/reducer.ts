import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Task } from 'shared/types'
import { getTasks } from './actions'

export interface State {
    getTasks: StateObject<Task[]>
}

const initialState: State = {
    getTasks: stateObject.getInitial([]),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getTasks.pending, state => {
      stateObject.setPending(state.getTasks)
    })
    .addCase(getTasks.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getTasks, action.payload)
    })
    .addCase(getTasks.rejected, (state, action) => {
      stateObject.setFailed(state.getTasks, action.error.message)
    })
)
