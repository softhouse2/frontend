import { RootState } from 'app'

export const getTasksSelector = (state: RootState) =>
  state.common.tasks.getTasks
