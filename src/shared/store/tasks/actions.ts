import { createAsyncThunk } from '@reduxjs/toolkit'
import Tasks, { GetTasksParams } from 'shared/services/tasks'

const tasks = new Tasks()

export const getTasks = createAsyncThunk(
    'getTasks',
    (payload: GetTasksParams) => tasks.getTasks(payload)
  )