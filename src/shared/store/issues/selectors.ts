import { RootState } from 'app'

export const getIssuesSelector = (state: RootState) =>
  state.common.issues.getIssues
