import { createAsyncThunk } from '@reduxjs/toolkit'
import Issues, { GetIssuesParams } from 'shared/services/issues'

const issues = new Issues()

export const getIssues = createAsyncThunk(
  'getIssues',
  (payload: GetIssuesParams) => issues.getIssues(payload)
)
