import { createReducer } from '@reduxjs/toolkit'
import { StateObject, stateObject } from 'shared/redux'
import { Issue } from 'shared/types'
import { getIssues } from './actions'

export interface State {
  getIssues: StateObject<Issue[]>
}

const initialState: State = {
  getIssues: stateObject.getInitial([]),
}

export default createReducer(initialState, builder =>
  builder
    .addCase(getIssues.pending, state => {
      stateObject.setPending(state.getIssues)
    })
    .addCase(getIssues.fulfilled, (state, action) => {
      stateObject.setSucceeded(state.getIssues, action.payload)
    })
    .addCase(getIssues.rejected, (state, action) => {
      stateObject.setFailed(state.getIssues, action.error.message)
    })
)
