import { StrictMode, Suspense } from 'react'
import { createRoot } from 'react-dom/client'
import App from './app'
import { Loader } from 'shared/components'

const rootElement = document.getElementById('root')
const root = createRoot(rootElement!)

root.render(
  <StrictMode>
    <Suspense fallback={<Loader />}>
      <App />
    </Suspense>
  </StrictMode>
)
