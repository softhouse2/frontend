const paths = {
  login: '/login',
  members: '/members',
  createMember: '/members/create',
  editMember: '/members/:memberId',

  requests: '/requests',
  createRequest: '/requests/create',
  editRequest: '/requests/:requestId',

  issues: '/issues',
  addIssue: '/requests/:requestId/add-issue',
  // editIssue: '/requests/:requestId/edit-issue/:issueId',
  editIssue: '/issues/:issueId',

  tasks: '/tasks',
  addTask: '/issues/:issueId/add-task',
  editTask: '/tasks/:taskId',

  clients: '/clients',
  createClient: '/clients/create',
  editClient: '/clients/:clientId',
  createSystemVersion: '/clients/:clientId/systemVer',
  editSystemVersion: '/clients/:clientId/systemVer/:systemVersionId',

  systems: '/systems',
  createSystem: '/systems/create',
  editSystem: '/systems/:systemId',
}

export default paths
